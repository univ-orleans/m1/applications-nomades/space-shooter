# Adaptive Performance
The Adaptive Performance package provides an API to get feedback about the thermal and power state of mobile devices, enabling applications to make performance-relevant adaptions at runtime.

# Installation
The Adaptive Performance package requires a provider packages such as the *Adaptive Performance Samsung (Android)* package to be installed via the provider section in the Adaptive Performance settings (or via Package Manager).
