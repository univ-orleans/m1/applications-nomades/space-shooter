﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void JoystickPlayerExample::FixedUpdate()
extern void JoystickPlayerExample_FixedUpdate_m3D7D2B1BE8C20574423088EF0C338700444B04B2 (void);
// 0x00000002 System.Void JoystickPlayerExample::.ctor()
extern void JoystickPlayerExample__ctor_mEE8014E341485D43F0629DE22DB9F407E47FC49B (void);
// 0x00000003 System.Void JoystickSetterExample::ModeChanged(System.Int32)
extern void JoystickSetterExample_ModeChanged_m79A8E404B1B30FAEECF0A21A55933659F664249B (void);
// 0x00000004 System.Void JoystickSetterExample::AxisChanged(System.Int32)
extern void JoystickSetterExample_AxisChanged_mE5DDF00D9E76A95282555A47E8789961A708642F (void);
// 0x00000005 System.Void JoystickSetterExample::SnapX(System.Boolean)
extern void JoystickSetterExample_SnapX_mD9AF3A51530489B40CACB5C4CE390B7BB930D7C7 (void);
// 0x00000006 System.Void JoystickSetterExample::SnapY(System.Boolean)
extern void JoystickSetterExample_SnapY_m16DC4EBA6DAFCDDEF054FCF901169B33AB7B01BF (void);
// 0x00000007 System.Void JoystickSetterExample::Update()
extern void JoystickSetterExample_Update_m0DEB8289C3A0133C8FAE885C6698C6082C51E3F6 (void);
// 0x00000008 System.Void JoystickSetterExample::.ctor()
extern void JoystickSetterExample__ctor_m2E65CFF997EC9703B4049E1F3870FC0AB3739C84 (void);
// 0x00000009 System.Single Joystick::get_Horizontal()
extern void Joystick_get_Horizontal_m1AE640531EE5E28A63A8D5AC757F9753DDA56321 (void);
// 0x0000000A System.Single Joystick::get_Vertical()
extern void Joystick_get_Vertical_m56B4D1C75DABA23923EF2E9C20543858E90D23C2 (void);
// 0x0000000B UnityEngine.Vector2 Joystick::get_Direction()
extern void Joystick_get_Direction_mF002E7B698C393FF866864D4A552357C535D36C5 (void);
// 0x0000000C System.Single Joystick::get_HandleRange()
extern void Joystick_get_HandleRange_mF0843B8C3E187FB08DD7EFF63F4AA4E9D30F4C99 (void);
// 0x0000000D System.Void Joystick::set_HandleRange(System.Single)
extern void Joystick_set_HandleRange_m7C2C550DE23BA7D39DD2015EFAC0DBB53087E0DD (void);
// 0x0000000E System.Single Joystick::get_DeadZone()
extern void Joystick_get_DeadZone_m9CFD309045AF6FC6F40430F2E84B04AF644A7355 (void);
// 0x0000000F System.Void Joystick::set_DeadZone(System.Single)
extern void Joystick_set_DeadZone_m9A107FE7A8EF41E9FBEB6979B1B17FD79C3F127C (void);
// 0x00000010 AxisOptions Joystick::get_AxisOptions()
extern void Joystick_get_AxisOptions_m3098305D1A5F1F48444A1ADAEC7BD46E980E274B (void);
// 0x00000011 System.Void Joystick::set_AxisOptions(AxisOptions)
extern void Joystick_set_AxisOptions_m671D494CBF07962B24BF4024059715FA650BB9EF (void);
// 0x00000012 System.Boolean Joystick::get_SnapX()
extern void Joystick_get_SnapX_mE645B0DB8C99081261ED3DF264B9AB41E92769E6 (void);
// 0x00000013 System.Void Joystick::set_SnapX(System.Boolean)
extern void Joystick_set_SnapX_m710022BEA478442D17908F10F5BA53375705AC3B (void);
// 0x00000014 System.Boolean Joystick::get_SnapY()
extern void Joystick_get_SnapY_mF8086B253937812A6BF0BA6D0818313899CBA564 (void);
// 0x00000015 System.Void Joystick::set_SnapY(System.Boolean)
extern void Joystick_set_SnapY_mAD4C6843FD698B99D23F21C3A15D9CE928289508 (void);
// 0x00000016 System.Void Joystick::Start()
extern void Joystick_Start_m3B4EEAA0389B5CCCA1479ACC4A167376E74BC275 (void);
// 0x00000017 System.Void Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_mB2A665CAD2B74565B6A1ACE2CA5A98A66020CE18 (void);
// 0x00000018 System.Void Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_m1127276AFCEF63DE869AC5156DE7712810B6C46D (void);
// 0x00000019 System.Void Joystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void Joystick_HandleInput_m38CE2907CF406D1F4B327F197E0CCED1C6DD8CC7 (void);
// 0x0000001A System.Void Joystick::FormatInput()
extern void Joystick_FormatInput_m6EAB109EE0C7D5EB1389E2277AD72335EF140826 (void);
// 0x0000001B System.Single Joystick::SnapFloat(System.Single,AxisOptions)
extern void Joystick_SnapFloat_m0A47278C9A57AC4A6696C0C13450F8F404580C19 (void);
// 0x0000001C System.Void Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_m84FA57FCD3325BDEE02FED329B1C963C4DA2A037 (void);
// 0x0000001D UnityEngine.Vector2 Joystick::ScreenPointToAnchoredPosition(UnityEngine.Vector2)
extern void Joystick_ScreenPointToAnchoredPosition_mAD769BA610FABC0D9C47294736AB0832C558D3FD (void);
// 0x0000001E System.Void Joystick::.ctor()
extern void Joystick__ctor_mF7C14D62A9A6B3BD77F6365BB88DE406A9CE4E08 (void);
// 0x0000001F System.Single DynamicJoystick::get_MoveThreshold()
extern void DynamicJoystick_get_MoveThreshold_m58D7166511D10A9933A62403E6BD58A85A22FE11 (void);
// 0x00000020 System.Void DynamicJoystick::set_MoveThreshold(System.Single)
extern void DynamicJoystick_set_MoveThreshold_m5A53DE83993960EA2650FB12F596E8C48C561199 (void);
// 0x00000021 System.Void DynamicJoystick::Start()
extern void DynamicJoystick_Start_mA61574C0A57F93B6604DFD076E6D2B8959637FF6 (void);
// 0x00000022 System.Void DynamicJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerDown_m86184227C74C293693A120601730591FE892D477 (void);
// 0x00000023 System.Void DynamicJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void DynamicJoystick_OnPointerUp_m19326842BD55962349FA84108425A679F320166A (void);
// 0x00000024 System.Void DynamicJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void DynamicJoystick_HandleInput_m9E141917F78D7887B05AEE4933F803D43F8A82AA (void);
// 0x00000025 System.Void DynamicJoystick::.ctor()
extern void DynamicJoystick__ctor_m30E27EBE028214E9F514F3788463A74511DC651D (void);
// 0x00000026 System.Void FixedJoystick::.ctor()
extern void FixedJoystick__ctor_mC4A98EB3129E3091007AD83B5B93672E7E9151B6 (void);
// 0x00000027 System.Void FloatingJoystick::Start()
extern void FloatingJoystick_Start_m290C93EC665E6DB28F837EE06CA2606EBA99C016 (void);
// 0x00000028 System.Void FloatingJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerDown_m8C00303A73289775A64DC879CCC9182B5BC849C5 (void);
// 0x00000029 System.Void FloatingJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FloatingJoystick_OnPointerUp_m26F0DEE158E1947EE053FE21B01ADA413FF3D4A2 (void);
// 0x0000002A System.Void FloatingJoystick::.ctor()
extern void FloatingJoystick__ctor_mA6F4B5F4A0C881759BFAD91256D5BE5E90399F8F (void);
// 0x0000002B System.Single VariableJoystick::get_MoveThreshold()
extern void VariableJoystick_get_MoveThreshold_m417DCAF09D8B811441FED64D4AE2854B00217EFA (void);
// 0x0000002C System.Void VariableJoystick::set_MoveThreshold(System.Single)
extern void VariableJoystick_set_MoveThreshold_mBF85E683260C0609A921EE7A1AD636E34B8FF5A0 (void);
// 0x0000002D System.Void VariableJoystick::SetMode(JoystickType)
extern void VariableJoystick_SetMode_m600C4C6E1FA830CF452EE33155AD60671BBEB04C (void);
// 0x0000002E System.Void VariableJoystick::Start()
extern void VariableJoystick_Start_m6BDC051D277BBFA7C373186E2825DE5429219169 (void);
// 0x0000002F System.Void VariableJoystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerDown_m1C27A5FF5BB7149FB39CC27B69DCBF3FB4F45ADB (void);
// 0x00000030 System.Void VariableJoystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void VariableJoystick_OnPointerUp_mFD7BF52D93B0B573E9227D5E971778EC003A1AC2 (void);
// 0x00000031 System.Void VariableJoystick::HandleInput(System.Single,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Camera)
extern void VariableJoystick_HandleInput_m868BD21C589C135310923B8C794AD5BA316C2D3A (void);
// 0x00000032 System.Void VariableJoystick::.ctor()
extern void VariableJoystick__ctor_m1E03B9859AAB8461C7A42706AE4E31606969C25F (void);
// 0x00000033 System.Void MoveBackground::Start()
extern void MoveBackground_Start_m73952C44ECDB8E02A7C84CE140363C3158096CD3 (void);
// 0x00000034 System.Void MoveBackground::Update()
extern void MoveBackground_Update_m233EF7D84EBF4ED003A6EC2F9942DD701BABDC2E (void);
// 0x00000035 System.Void MoveBackground::.ctor()
extern void MoveBackground__ctor_mAF0741CCE7230F5038EA97EDD54679F1D20DF22D (void);
// 0x00000036 System.Void ScrollBackground::Start()
extern void ScrollBackground_Start_m6C6E6A47F1F0915E5ADF9733EA33BB8CAA5D558A (void);
// 0x00000037 System.Void ScrollBackground::Update()
extern void ScrollBackground_Update_m00D44D6E0B21CF0DFAE601DC0EB6FA2B0A2EE237 (void);
// 0x00000038 System.Void ScrollBackground::.ctor()
extern void ScrollBackground__ctor_mF2E8B5FA4341B1B06A501DC440FBC8B5BAE29380 (void);
// 0x00000039 System.Void JoyButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void JoyButton_OnPointerDown_m027B5B019F1BAD1F99AD35C8DCDBA123A059A876 (void);
// 0x0000003A System.Void JoyButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void JoyButton_OnPointerUp_mDD38F4774DEB3806D3097DA6770075A2195B8735 (void);
// 0x0000003B System.Void JoyButton::.ctor()
extern void JoyButton__ctor_m4D1DFEA5F31EB8C4F8204768CA329007191AE135 (void);
// 0x0000003C System.Void DestructionEffectEnemy::Start()
extern void DestructionEffectEnemy_Start_m2A239827B2CEC541399FB49A8FFB86846AABDFA0 (void);
// 0x0000003D System.Void DestructionEffectEnemy::Update()
extern void DestructionEffectEnemy_Update_m8ABC902696206AEBE0FC84448F6180A408BA6D54 (void);
// 0x0000003E System.Void DestructionEffectEnemy::.ctor()
extern void DestructionEffectEnemy__ctor_mE3392720B83AE27F0062F301A20B73C55DA1DD02 (void);
// 0x0000003F System.Void ManageEnemy::Start()
extern void ManageEnemy_Start_mE121432A329522875EE8D1E5DF84410B6F64FFEC (void);
// 0x00000040 System.Void ManageEnemy::Update()
extern void ManageEnemy_Update_m2C1F82864863B910C60D749433D7D2208920DE3F (void);
// 0x00000041 UnityEngine.Vector3 ManageEnemy::RandomPosition(UnityEngine.Vector3)
extern void ManageEnemy_RandomPosition_mAB1D66F48604EEBFD52F246843DD5DAE7E62086D (void);
// 0x00000042 System.Void ManageEnemy::.ctor()
extern void ManageEnemy__ctor_m83E39BE85DA2DC1ACC713D9CBE2351CA24F14E2B (void);
// 0x00000043 System.Void MoveMeteor::Start()
extern void MoveMeteor_Start_mAC824C78251B2E173E463ABE962D804D035D739B (void);
// 0x00000044 System.Void MoveMeteor::Update()
extern void MoveMeteor_Update_m01742ABCA5719175AD642EB4C69A19AC2F343F24 (void);
// 0x00000045 System.Void MoveMeteor::.ctor()
extern void MoveMeteor__ctor_mAA0099A3BE88B0C4D442F25889548CB76968CC9F (void);
// 0x00000046 System.Void ManageAttributes::Start()
extern void ManageAttributes_Start_m42979D217C33A43A157787CA782057713CC25D82 (void);
// 0x00000047 System.Int32 ManageAttributes::AttackedBy(UnityEngine.GameObject)
extern void ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9 (void);
// 0x00000048 System.Int32 ManageAttributes::getLifePoints()
extern void ManageAttributes_getLifePoints_mA744A7D2223A6FD87137A28794FE7B20F4EC79E3 (void);
// 0x00000049 System.Void ManageAttributes::RestartLife()
extern void ManageAttributes_RestartLife_mE1F2FB13FDD3F458547FF98DB025C247C114CD43 (void);
// 0x0000004A System.Void ManageAttributes::.ctor()
extern void ManageAttributes__ctor_mB772E6BCEBF8BFF7D0941864A188BFBCD3889569 (void);
// 0x0000004B System.Void ManageGame::Start()
extern void ManageGame_Start_m9216CD6AE375098C1CB437ABA04CA63CEA5EAAEE (void);
// 0x0000004C System.Void ManageGame::Update()
extern void ManageGame_Update_mA0A47EFDEDB14FEE6256A7CE372F0867E034DCEA (void);
// 0x0000004D System.Void ManageGame::ShowScene(System.String)
extern void ManageGame_ShowScene_m2D10543862AA8077F31C20FBAE91933A7920E318 (void);
// 0x0000004E System.Void ManageGame::QuitGame()
extern void ManageGame_QuitGame_mD886C1B313AC14E2D780E85A772C3438CF55D94E (void);
// 0x0000004F System.Void ManageGame::PauseGameLevel()
extern void ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4 (void);
// 0x00000050 System.Void ManageGame::ContinueGameLevel()
extern void ManageGame_ContinueGameLevel_m9371DB8D650C9A6372522C0E1F99FF24413E3A99 (void);
// 0x00000051 System.Void ManageGame::StopGameLevel()
extern void ManageGame_StopGameLevel_m6D77388869CA672CC333DA0933B88B1F306A87F3 (void);
// 0x00000052 System.Void ManageGame::SetIsPlaying(System.Boolean)
extern void ManageGame_SetIsPlaying_m311E3C8F87C1D94BB98C5B1864CC89F749C20D39 (void);
// 0x00000053 System.Void ManageGame::AddToLife(System.Int32)
extern void ManageGame_AddToLife_mE5BEB7212250BC516C40748733B2B878DAA46C53 (void);
// 0x00000054 System.Void ManageGame::RemoveToLife(System.Int32)
extern void ManageGame_RemoveToLife_m2477ECBE9B0ACB02CF359F2E4564E46797CB805F (void);
// 0x00000055 System.Int32 ManageGame::GetLife()
extern void ManageGame_GetLife_mD2160B685B50166139A7D58F943FB66F9549ECDA (void);
// 0x00000056 System.Void ManageGame::AddToScore(System.Int32)
extern void ManageGame_AddToScore_m13E78E7815D9EA8CFB47B9E3EE45E81E54E6776D (void);
// 0x00000057 System.Int32 ManageGame::GetScore()
extern void ManageGame_GetScore_m4D8E4E35E619BED7B648DE885FF43AC10ACAD14F (void);
// 0x00000058 System.Void ManageGame::.ctor()
extern void ManageGame__ctor_mA6B7A7CE4D286460F77AB226E334C14B0CC2E780 (void);
// 0x00000059 System.Void ManageLevel::Start()
extern void ManageLevel_Start_mDF701DCE54B346FE476312A3880C1815F242C431 (void);
// 0x0000005A System.Void ManageLevel::Update()
extern void ManageLevel_Update_m984A5DB5426E1B0082A033E2B0338E8E47A49191 (void);
// 0x0000005B System.Void ManageLevel::StartCurrentLevel()
extern void ManageLevel_StartCurrentLevel_mBADABC58A9C3DE01058CC5652E15BF250B1F1AFA (void);
// 0x0000005C System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<ManageEnemy>> ManageLevel::GetSections(UnityEngine.GameObject)
extern void ManageLevel_GetSections_m16E14CCBEE0AE6AAC3F96DDE1334C61DE2EC3646 (void);
// 0x0000005D System.Collections.Generic.Dictionary`2<System.Int32,System.Single> ManageLevel::GetDurations(System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<ManageEnemy>>)
extern void ManageLevel_GetDurations_m0705FD31331020761ED3209D2789A65723C23EDD (void);
// 0x0000005E System.Collections.IEnumerator ManageLevel::EnableManager(ManageEnemy,System.Boolean,System.Single)
extern void ManageLevel_EnableManager_mF82737D6CD0661527EC66C22A485E06B07E051DA (void);
// 0x0000005F System.Collections.IEnumerator ManageLevel::ActiveNextLevel(System.Single)
extern void ManageLevel_ActiveNextLevel_mB30A5F8AD99DA31F2F058F2218DED3F7D118525A (void);
// 0x00000060 System.Void ManageLevel::.ctor()
extern void ManageLevel__ctor_mDAF8CAA24AB2B03FD0E85120E5659C58132391F7 (void);
// 0x00000061 System.Void ManageLevel/<>c::.cctor()
extern void U3CU3Ec__cctor_m98E44698C7BD4ADB1547210B02B2749E9C2C1C5B (void);
// 0x00000062 System.Void ManageLevel/<>c::.ctor()
extern void U3CU3Ec__ctor_mDA6E034B3761B78440CD634BEEF1B6B710C541EC (void);
// 0x00000063 System.Int32 ManageLevel/<>c::<GetSections>b__14_0(ManageEnemy,ManageEnemy)
extern void U3CU3Ec_U3CGetSectionsU3Eb__14_0_m3B5A686129812AD57BA49BB2032FE666CEF544C4 (void);
// 0x00000064 System.Single ManageLevel/<>c::<GetDurations>b__15_0(ManageEnemy)
extern void U3CU3Ec_U3CGetDurationsU3Eb__15_0_m2C66615382E14766ED18FA7500A7630BFDF2C8D6 (void);
// 0x00000065 System.Single ManageLevel/<>c::<GetDurations>b__15_1(ManageEnemy)
extern void U3CU3Ec_U3CGetDurationsU3Eb__15_1_m790EFADBDEA985FB6A550CF2268AB4C865625511 (void);
// 0x00000066 System.Void ManageLevel/<EnableManager>d__16::.ctor(System.Int32)
extern void U3CEnableManagerU3Ed__16__ctor_mAB055A66981CC86363D6EF4891D5A05C2E86BA16 (void);
// 0x00000067 System.Void ManageLevel/<EnableManager>d__16::System.IDisposable.Dispose()
extern void U3CEnableManagerU3Ed__16_System_IDisposable_Dispose_mCF3FEB9A66CD98AA8D0E2A8E0865BC9D9DCD9732 (void);
// 0x00000068 System.Boolean ManageLevel/<EnableManager>d__16::MoveNext()
extern void U3CEnableManagerU3Ed__16_MoveNext_mE0A2E613FA0A6B3424FE7D032F31A61FB255D896 (void);
// 0x00000069 System.Object ManageLevel/<EnableManager>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnableManagerU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64230403FE75B0FA3F17E687B4C83C3E9F18CCA1 (void);
// 0x0000006A System.Void ManageLevel/<EnableManager>d__16::System.Collections.IEnumerator.Reset()
extern void U3CEnableManagerU3Ed__16_System_Collections_IEnumerator_Reset_mA05DC01F6EFB5B2FA46C4CF4F223A1B5589CB42F (void);
// 0x0000006B System.Object ManageLevel/<EnableManager>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CEnableManagerU3Ed__16_System_Collections_IEnumerator_get_Current_m4D4EAEAE0F4E81736F77396D58FDE0D0C9A374D6 (void);
// 0x0000006C System.Void ManageLevel/<ActiveNextLevel>d__17::.ctor(System.Int32)
extern void U3CActiveNextLevelU3Ed__17__ctor_mC550EBE01F3B4C7FABE454FA790B18ADA5C72C9D (void);
// 0x0000006D System.Void ManageLevel/<ActiveNextLevel>d__17::System.IDisposable.Dispose()
extern void U3CActiveNextLevelU3Ed__17_System_IDisposable_Dispose_mCC524CE0ACC825AC3A0A3FC3B99BD2CDC5B96006 (void);
// 0x0000006E System.Boolean ManageLevel/<ActiveNextLevel>d__17::MoveNext()
extern void U3CActiveNextLevelU3Ed__17_MoveNext_m9A8B06D33151DC9A09B61996B8274E45394DE138 (void);
// 0x0000006F System.Object ManageLevel/<ActiveNextLevel>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActiveNextLevelU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01DC891D3D33EC4A2E57B5D1E30906D980EF2859 (void);
// 0x00000070 System.Void ManageLevel/<ActiveNextLevel>d__17::System.Collections.IEnumerator.Reset()
extern void U3CActiveNextLevelU3Ed__17_System_Collections_IEnumerator_Reset_mC0252F965FBEE7F0285DB3381DD016148893CAC6 (void);
// 0x00000071 System.Object ManageLevel/<ActiveNextLevel>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CActiveNextLevelU3Ed__17_System_Collections_IEnumerator_get_Current_m24AFBDF5C04C75C0C115D57723AB3B0C7F9D8AFD (void);
// 0x00000072 System.Void ManageSound::Start()
extern void ManageSound_Start_mD180C83B21A1A63D1698FAF6E09B25A8C734F461 (void);
// 0x00000073 System.Void ManageSound::ImpactMeteor()
extern void ManageSound_ImpactMeteor_mAE790A3E978351A2CF9AE0E3BD1BFF1651A7ADF4 (void);
// 0x00000074 System.Void ManageSound::ImpactShip()
extern void ManageSound_ImpactShip_m925097827CEB2058B2B34C5382BCB10CC2DFB038 (void);
// 0x00000075 System.Void ManageSound::ShootLaser()
extern void ManageSound_ShootLaser_mA62D9143408A70AC587982CC6962A476BAD12BFB (void);
// 0x00000076 System.Void ManageSound::MakeSound(UnityEngine.AudioClip)
extern void ManageSound_MakeSound_m47DBE4EBB85A41CCD912BE7CFDC883E6713A533A (void);
// 0x00000077 System.Void ManageSound::.ctor()
extern void ManageSound__ctor_mA75BA6C257091C1B66C2F621FE02221A60474B27 (void);
// 0x00000078 System.Void SetText::Set(System.Int32)
extern void SetText_Set_m56E9F22D9E3D18D7C1CD9620DD1455A198353231 (void);
// 0x00000079 System.Void SetText::.ctor()
extern void SetText__ctor_m63189AA0BAE1821C3E0212AA07E2FA83983B47E5 (void);
// 0x0000007A System.Void ViewBorders::Start()
extern void ViewBorders_Start_m29C7D0AB22F9C7863C2A48AA568D023FA069CC83 (void);
// 0x0000007B System.Single ViewBorders::borderTop()
extern void ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5 (void);
// 0x0000007C System.Single ViewBorders::borderBottom()
extern void ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6 (void);
// 0x0000007D System.Single ViewBorders::borderLeft()
extern void ViewBorders_borderLeft_m7C50A44636CE5AEEC9BE685BF200E100A5CA02C1 (void);
// 0x0000007E System.Single ViewBorders::borderRight()
extern void ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B (void);
// 0x0000007F System.Single ViewBorders::width()
extern void ViewBorders_width_mCC7441088FE256358A4A5A2886B7AD8F371060F5 (void);
// 0x00000080 System.Single ViewBorders::height()
extern void ViewBorders_height_m3245C67837CD16823E47CDE4B29855FBF24D35F6 (void);
// 0x00000081 System.Boolean ViewBorders::isOnBorder(UnityEngine.GameObject)
extern void ViewBorders_isOnBorder_m05F87216D6551D8CC695E4B856C9FB201D1C4E26 (void);
// 0x00000082 System.Boolean ViewBorders::isOut(UnityEngine.GameObject)
extern void ViewBorders_isOut_m0A983F94132A2262E813A7FD17DDC6B666A5AC3C (void);
// 0x00000083 System.Boolean ViewBorders::isOnBorderTop(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314 (void);
// 0x00000084 System.Boolean ViewBorders::isOnBorderBottom(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB (void);
// 0x00000085 System.Boolean ViewBorders::isOnBorderLeft(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9 (void);
// 0x00000086 System.Boolean ViewBorders::isOnBorderRight(UnityEngine.GameObject)
extern void ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5 (void);
// 0x00000087 System.Boolean ViewBorders::isOutFromTop(UnityEngine.GameObject)
extern void ViewBorders_isOutFromTop_mE6047837B4FFA21B6B70486AF6821944D9A44EAA (void);
// 0x00000088 System.Boolean ViewBorders::isOutFromBottom(UnityEngine.GameObject)
extern void ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622 (void);
// 0x00000089 System.Boolean ViewBorders::isOutFromLeft(UnityEngine.GameObject)
extern void ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC (void);
// 0x0000008A System.Boolean ViewBorders::isOutFromRight(UnityEngine.GameObject)
extern void ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2 (void);
// 0x0000008B System.Single ViewBorders::getSizeY(UnityEngine.GameObject)
extern void ViewBorders_getSizeY_m2EA0024C500F78EF93ED7C6FFA9D2BEDE02240AA (void);
// 0x0000008C System.Single ViewBorders::getSizeX(UnityEngine.GameObject)
extern void ViewBorders_getSizeX_mDDCA0C0A9F4485522E0260DA802CBA86C613F28F (void);
// 0x0000008D System.Void ViewBorders::.ctor()
extern void ViewBorders__ctor_m7B2ADBB9D3D1CCBF60F565A10813D46D95F50561 (void);
// 0x0000008E System.Void DestructionEffectLaser::Start()
extern void DestructionEffectLaser_Start_mEE40BDCFAA8606867D4DE10BE748DBC686967E23 (void);
// 0x0000008F System.Void DestructionEffectLaser::Update()
extern void DestructionEffectLaser_Update_m8E52C4FE4399EEB7E6151DFCAB4B134BD5C268D5 (void);
// 0x00000090 System.Void DestructionEffectLaser::.ctor()
extern void DestructionEffectLaser__ctor_mF75384A5D028D953963E438F45B0C54ED26B0D1F (void);
// 0x00000091 System.Void MoveLaser1::Start()
extern void MoveLaser1_Start_m65994D75F536382A7BCEC75AD2CBBFE6A0395EFC (void);
// 0x00000092 System.Void MoveLaser1::Update()
extern void MoveLaser1_Update_mB14CB11907F598D69B541F78D97EF9613CFF234B (void);
// 0x00000093 System.Void MoveLaser1::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void MoveLaser1_OnCollisionEnter2D_mE7A09A80DDF61607DA9902FCAC475D17F8805371 (void);
// 0x00000094 System.Void MoveLaser1::.ctor()
extern void MoveLaser1__ctor_m891E52378394329C7912E59536B4D5AED2E2C429 (void);
// 0x00000095 System.Void ShootLaser1::Start()
extern void ShootLaser1_Start_mC9E08BDEDABA139120B21BA3A23B08E3490CDCFE (void);
// 0x00000096 System.Void ShootLaser1::Update()
extern void ShootLaser1_Update_mA57094A89B4E1DFCCBB74B3FDD62A63F8A38AD35 (void);
// 0x00000097 System.Void ShootLaser1::Shoot()
extern void ShootLaser1_Shoot_m888912CA48BE2061D167B0665DEF96EE3A0D8AFA (void);
// 0x00000098 System.Void ShootLaser1::.ctor()
extern void ShootLaser1__ctor_mA41F457EBC289D3DB39825F1C7ADCC529437630E (void);
// 0x00000099 System.Void MoveShip::Start()
extern void MoveShip_Start_m633249C1BDACD931611617D01EF31FE21B804819 (void);
// 0x0000009A System.Void MoveShip::Update()
extern void MoveShip_Update_mA926EB5714CE554E6AD2264E888E44D8756E30A2 (void);
// 0x0000009B UnityEngine.Vector2 MoveShip::getInput()
extern void MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749 (void);
// 0x0000009C System.Void MoveShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void MoveShip_OnCollisionEnter2D_m49C54471E2C7F1AD2780BECFCD186FA8E322CFEC (void);
// 0x0000009D System.Void MoveShip::OnCollisionExit2D(UnityEngine.Collision2D)
extern void MoveShip_OnCollisionExit2D_m86C77026B656E4D502D73BC55F0E946A4F3F92EE (void);
// 0x0000009E System.Void MoveShip::.ctor()
extern void MoveShip__ctor_m6DDFD05FA781A523D7E648BAE8669F7836FD8ADA (void);
// 0x0000009F System.Void PropulsionEffect::Start()
extern void PropulsionEffect_Start_m5B5C5CF51642E66D1DA10F9A8814B042D4F73BE4 (void);
// 0x000000A0 System.Void PropulsionEffect::Update()
extern void PropulsionEffect_Update_m1F2A89E9B46D24D010D6F7A150193A4304A29145 (void);
// 0x000000A1 System.Void PropulsionEffect::.ctor()
extern void PropulsionEffect__ctor_mA7C23DCB8BB410B81AE781CD1A4A56161ECE8FC5 (void);
// 0x000000A2 System.Void ManageLifeBar::Start()
extern void ManageLifeBar_Start_m44A0AA415C799AC8D66D3BE10A703D781C93688F (void);
// 0x000000A3 System.Void ManageLifeBar::Update()
extern void ManageLifeBar_Update_mD0FBC92BCE43B59C8A5D87643662D894D00D2CEF (void);
// 0x000000A4 System.Void ManageLifeBar::AddToLife(System.Int32)
extern void ManageLifeBar_AddToLife_m89DF759F658CB7AB76CD8E059C34E93D04E166E3 (void);
// 0x000000A5 System.Void ManageLifeBar::RemoveToLife(System.Int32)
extern void ManageLifeBar_RemoveToLife_mD92710734AE395928F87BFE55661FB8AA6A1368D (void);
// 0x000000A6 System.Int32 ManageLifeBar::GetLife()
extern void ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8 (void);
// 0x000000A7 System.Void ManageLifeBar::.ctor()
extern void ManageLifeBar__ctor_m4F28E430C644E499708D8ABBF307B37230DB4B7B (void);
// 0x000000A8 System.Void ManagerScore::Update()
extern void ManagerScore_Update_mB614E9A52C210E11FF01CCE6CBCED218E7C0C9C9 (void);
// 0x000000A9 System.Void ManagerScore::AddToScore(System.Int32)
extern void ManagerScore_AddToScore_m0AC27C2FCF200C3CCCAA432A6B572495FE9F5775 (void);
// 0x000000AA System.Int32 ManagerScore::GetScore()
extern void ManagerScore_GetScore_mE526A8E73089A0AE6C1AB90B84D8363DF2CEA16C (void);
// 0x000000AB System.Void ManagerScore::.ctor()
extern void ManagerScore__ctor_m2817EA953C04C9D3D29D0423A40954A5A410EC21 (void);
static Il2CppMethodPointer s_methodPointers[171] = 
{
	JoystickPlayerExample_FixedUpdate_m3D7D2B1BE8C20574423088EF0C338700444B04B2,
	JoystickPlayerExample__ctor_mEE8014E341485D43F0629DE22DB9F407E47FC49B,
	JoystickSetterExample_ModeChanged_m79A8E404B1B30FAEECF0A21A55933659F664249B,
	JoystickSetterExample_AxisChanged_mE5DDF00D9E76A95282555A47E8789961A708642F,
	JoystickSetterExample_SnapX_mD9AF3A51530489B40CACB5C4CE390B7BB930D7C7,
	JoystickSetterExample_SnapY_m16DC4EBA6DAFCDDEF054FCF901169B33AB7B01BF,
	JoystickSetterExample_Update_m0DEB8289C3A0133C8FAE885C6698C6082C51E3F6,
	JoystickSetterExample__ctor_m2E65CFF997EC9703B4049E1F3870FC0AB3739C84,
	Joystick_get_Horizontal_m1AE640531EE5E28A63A8D5AC757F9753DDA56321,
	Joystick_get_Vertical_m56B4D1C75DABA23923EF2E9C20543858E90D23C2,
	Joystick_get_Direction_mF002E7B698C393FF866864D4A552357C535D36C5,
	Joystick_get_HandleRange_mF0843B8C3E187FB08DD7EFF63F4AA4E9D30F4C99,
	Joystick_set_HandleRange_m7C2C550DE23BA7D39DD2015EFAC0DBB53087E0DD,
	Joystick_get_DeadZone_m9CFD309045AF6FC6F40430F2E84B04AF644A7355,
	Joystick_set_DeadZone_m9A107FE7A8EF41E9FBEB6979B1B17FD79C3F127C,
	Joystick_get_AxisOptions_m3098305D1A5F1F48444A1ADAEC7BD46E980E274B,
	Joystick_set_AxisOptions_m671D494CBF07962B24BF4024059715FA650BB9EF,
	Joystick_get_SnapX_mE645B0DB8C99081261ED3DF264B9AB41E92769E6,
	Joystick_set_SnapX_m710022BEA478442D17908F10F5BA53375705AC3B,
	Joystick_get_SnapY_mF8086B253937812A6BF0BA6D0818313899CBA564,
	Joystick_set_SnapY_mAD4C6843FD698B99D23F21C3A15D9CE928289508,
	Joystick_Start_m3B4EEAA0389B5CCCA1479ACC4A167376E74BC275,
	Joystick_OnPointerDown_mB2A665CAD2B74565B6A1ACE2CA5A98A66020CE18,
	Joystick_OnDrag_m1127276AFCEF63DE869AC5156DE7712810B6C46D,
	Joystick_HandleInput_m38CE2907CF406D1F4B327F197E0CCED1C6DD8CC7,
	Joystick_FormatInput_m6EAB109EE0C7D5EB1389E2277AD72335EF140826,
	Joystick_SnapFloat_m0A47278C9A57AC4A6696C0C13450F8F404580C19,
	Joystick_OnPointerUp_m84FA57FCD3325BDEE02FED329B1C963C4DA2A037,
	Joystick_ScreenPointToAnchoredPosition_mAD769BA610FABC0D9C47294736AB0832C558D3FD,
	Joystick__ctor_mF7C14D62A9A6B3BD77F6365BB88DE406A9CE4E08,
	DynamicJoystick_get_MoveThreshold_m58D7166511D10A9933A62403E6BD58A85A22FE11,
	DynamicJoystick_set_MoveThreshold_m5A53DE83993960EA2650FB12F596E8C48C561199,
	DynamicJoystick_Start_mA61574C0A57F93B6604DFD076E6D2B8959637FF6,
	DynamicJoystick_OnPointerDown_m86184227C74C293693A120601730591FE892D477,
	DynamicJoystick_OnPointerUp_m19326842BD55962349FA84108425A679F320166A,
	DynamicJoystick_HandleInput_m9E141917F78D7887B05AEE4933F803D43F8A82AA,
	DynamicJoystick__ctor_m30E27EBE028214E9F514F3788463A74511DC651D,
	FixedJoystick__ctor_mC4A98EB3129E3091007AD83B5B93672E7E9151B6,
	FloatingJoystick_Start_m290C93EC665E6DB28F837EE06CA2606EBA99C016,
	FloatingJoystick_OnPointerDown_m8C00303A73289775A64DC879CCC9182B5BC849C5,
	FloatingJoystick_OnPointerUp_m26F0DEE158E1947EE053FE21B01ADA413FF3D4A2,
	FloatingJoystick__ctor_mA6F4B5F4A0C881759BFAD91256D5BE5E90399F8F,
	VariableJoystick_get_MoveThreshold_m417DCAF09D8B811441FED64D4AE2854B00217EFA,
	VariableJoystick_set_MoveThreshold_mBF85E683260C0609A921EE7A1AD636E34B8FF5A0,
	VariableJoystick_SetMode_m600C4C6E1FA830CF452EE33155AD60671BBEB04C,
	VariableJoystick_Start_m6BDC051D277BBFA7C373186E2825DE5429219169,
	VariableJoystick_OnPointerDown_m1C27A5FF5BB7149FB39CC27B69DCBF3FB4F45ADB,
	VariableJoystick_OnPointerUp_mFD7BF52D93B0B573E9227D5E971778EC003A1AC2,
	VariableJoystick_HandleInput_m868BD21C589C135310923B8C794AD5BA316C2D3A,
	VariableJoystick__ctor_m1E03B9859AAB8461C7A42706AE4E31606969C25F,
	MoveBackground_Start_m73952C44ECDB8E02A7C84CE140363C3158096CD3,
	MoveBackground_Update_m233EF7D84EBF4ED003A6EC2F9942DD701BABDC2E,
	MoveBackground__ctor_mAF0741CCE7230F5038EA97EDD54679F1D20DF22D,
	ScrollBackground_Start_m6C6E6A47F1F0915E5ADF9733EA33BB8CAA5D558A,
	ScrollBackground_Update_m00D44D6E0B21CF0DFAE601DC0EB6FA2B0A2EE237,
	ScrollBackground__ctor_mF2E8B5FA4341B1B06A501DC440FBC8B5BAE29380,
	JoyButton_OnPointerDown_m027B5B019F1BAD1F99AD35C8DCDBA123A059A876,
	JoyButton_OnPointerUp_mDD38F4774DEB3806D3097DA6770075A2195B8735,
	JoyButton__ctor_m4D1DFEA5F31EB8C4F8204768CA329007191AE135,
	DestructionEffectEnemy_Start_m2A239827B2CEC541399FB49A8FFB86846AABDFA0,
	DestructionEffectEnemy_Update_m8ABC902696206AEBE0FC84448F6180A408BA6D54,
	DestructionEffectEnemy__ctor_mE3392720B83AE27F0062F301A20B73C55DA1DD02,
	ManageEnemy_Start_mE121432A329522875EE8D1E5DF84410B6F64FFEC,
	ManageEnemy_Update_m2C1F82864863B910C60D749433D7D2208920DE3F,
	ManageEnemy_RandomPosition_mAB1D66F48604EEBFD52F246843DD5DAE7E62086D,
	ManageEnemy__ctor_m83E39BE85DA2DC1ACC713D9CBE2351CA24F14E2B,
	MoveMeteor_Start_mAC824C78251B2E173E463ABE962D804D035D739B,
	MoveMeteor_Update_m01742ABCA5719175AD642EB4C69A19AC2F343F24,
	MoveMeteor__ctor_mAA0099A3BE88B0C4D442F25889548CB76968CC9F,
	ManageAttributes_Start_m42979D217C33A43A157787CA782057713CC25D82,
	ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9,
	ManageAttributes_getLifePoints_mA744A7D2223A6FD87137A28794FE7B20F4EC79E3,
	ManageAttributes_RestartLife_mE1F2FB13FDD3F458547FF98DB025C247C114CD43,
	ManageAttributes__ctor_mB772E6BCEBF8BFF7D0941864A188BFBCD3889569,
	ManageGame_Start_m9216CD6AE375098C1CB437ABA04CA63CEA5EAAEE,
	ManageGame_Update_mA0A47EFDEDB14FEE6256A7CE372F0867E034DCEA,
	ManageGame_ShowScene_m2D10543862AA8077F31C20FBAE91933A7920E318,
	ManageGame_QuitGame_mD886C1B313AC14E2D780E85A772C3438CF55D94E,
	ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4,
	ManageGame_ContinueGameLevel_m9371DB8D650C9A6372522C0E1F99FF24413E3A99,
	ManageGame_StopGameLevel_m6D77388869CA672CC333DA0933B88B1F306A87F3,
	ManageGame_SetIsPlaying_m311E3C8F87C1D94BB98C5B1864CC89F749C20D39,
	ManageGame_AddToLife_mE5BEB7212250BC516C40748733B2B878DAA46C53,
	ManageGame_RemoveToLife_m2477ECBE9B0ACB02CF359F2E4564E46797CB805F,
	ManageGame_GetLife_mD2160B685B50166139A7D58F943FB66F9549ECDA,
	ManageGame_AddToScore_m13E78E7815D9EA8CFB47B9E3EE45E81E54E6776D,
	ManageGame_GetScore_m4D8E4E35E619BED7B648DE885FF43AC10ACAD14F,
	ManageGame__ctor_mA6B7A7CE4D286460F77AB226E334C14B0CC2E780,
	ManageLevel_Start_mDF701DCE54B346FE476312A3880C1815F242C431,
	ManageLevel_Update_m984A5DB5426E1B0082A033E2B0338E8E47A49191,
	ManageLevel_StartCurrentLevel_mBADABC58A9C3DE01058CC5652E15BF250B1F1AFA,
	ManageLevel_GetSections_m16E14CCBEE0AE6AAC3F96DDE1334C61DE2EC3646,
	ManageLevel_GetDurations_m0705FD31331020761ED3209D2789A65723C23EDD,
	ManageLevel_EnableManager_mF82737D6CD0661527EC66C22A485E06B07E051DA,
	ManageLevel_ActiveNextLevel_mB30A5F8AD99DA31F2F058F2218DED3F7D118525A,
	ManageLevel__ctor_mDAF8CAA24AB2B03FD0E85120E5659C58132391F7,
	U3CU3Ec__cctor_m98E44698C7BD4ADB1547210B02B2749E9C2C1C5B,
	U3CU3Ec__ctor_mDA6E034B3761B78440CD634BEEF1B6B710C541EC,
	U3CU3Ec_U3CGetSectionsU3Eb__14_0_m3B5A686129812AD57BA49BB2032FE666CEF544C4,
	U3CU3Ec_U3CGetDurationsU3Eb__15_0_m2C66615382E14766ED18FA7500A7630BFDF2C8D6,
	U3CU3Ec_U3CGetDurationsU3Eb__15_1_m790EFADBDEA985FB6A550CF2268AB4C865625511,
	U3CEnableManagerU3Ed__16__ctor_mAB055A66981CC86363D6EF4891D5A05C2E86BA16,
	U3CEnableManagerU3Ed__16_System_IDisposable_Dispose_mCF3FEB9A66CD98AA8D0E2A8E0865BC9D9DCD9732,
	U3CEnableManagerU3Ed__16_MoveNext_mE0A2E613FA0A6B3424FE7D032F31A61FB255D896,
	U3CEnableManagerU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64230403FE75B0FA3F17E687B4C83C3E9F18CCA1,
	U3CEnableManagerU3Ed__16_System_Collections_IEnumerator_Reset_mA05DC01F6EFB5B2FA46C4CF4F223A1B5589CB42F,
	U3CEnableManagerU3Ed__16_System_Collections_IEnumerator_get_Current_m4D4EAEAE0F4E81736F77396D58FDE0D0C9A374D6,
	U3CActiveNextLevelU3Ed__17__ctor_mC550EBE01F3B4C7FABE454FA790B18ADA5C72C9D,
	U3CActiveNextLevelU3Ed__17_System_IDisposable_Dispose_mCC524CE0ACC825AC3A0A3FC3B99BD2CDC5B96006,
	U3CActiveNextLevelU3Ed__17_MoveNext_m9A8B06D33151DC9A09B61996B8274E45394DE138,
	U3CActiveNextLevelU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01DC891D3D33EC4A2E57B5D1E30906D980EF2859,
	U3CActiveNextLevelU3Ed__17_System_Collections_IEnumerator_Reset_mC0252F965FBEE7F0285DB3381DD016148893CAC6,
	U3CActiveNextLevelU3Ed__17_System_Collections_IEnumerator_get_Current_m24AFBDF5C04C75C0C115D57723AB3B0C7F9D8AFD,
	ManageSound_Start_mD180C83B21A1A63D1698FAF6E09B25A8C734F461,
	ManageSound_ImpactMeteor_mAE790A3E978351A2CF9AE0E3BD1BFF1651A7ADF4,
	ManageSound_ImpactShip_m925097827CEB2058B2B34C5382BCB10CC2DFB038,
	ManageSound_ShootLaser_mA62D9143408A70AC587982CC6962A476BAD12BFB,
	ManageSound_MakeSound_m47DBE4EBB85A41CCD912BE7CFDC883E6713A533A,
	ManageSound__ctor_mA75BA6C257091C1B66C2F621FE02221A60474B27,
	SetText_Set_m56E9F22D9E3D18D7C1CD9620DD1455A198353231,
	SetText__ctor_m63189AA0BAE1821C3E0212AA07E2FA83983B47E5,
	ViewBorders_Start_m29C7D0AB22F9C7863C2A48AA568D023FA069CC83,
	ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5,
	ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6,
	ViewBorders_borderLeft_m7C50A44636CE5AEEC9BE685BF200E100A5CA02C1,
	ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B,
	ViewBorders_width_mCC7441088FE256358A4A5A2886B7AD8F371060F5,
	ViewBorders_height_m3245C67837CD16823E47CDE4B29855FBF24D35F6,
	ViewBorders_isOnBorder_m05F87216D6551D8CC695E4B856C9FB201D1C4E26,
	ViewBorders_isOut_m0A983F94132A2262E813A7FD17DDC6B666A5AC3C,
	ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314,
	ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB,
	ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9,
	ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5,
	ViewBorders_isOutFromTop_mE6047837B4FFA21B6B70486AF6821944D9A44EAA,
	ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622,
	ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC,
	ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2,
	ViewBorders_getSizeY_m2EA0024C500F78EF93ED7C6FFA9D2BEDE02240AA,
	ViewBorders_getSizeX_mDDCA0C0A9F4485522E0260DA802CBA86C613F28F,
	ViewBorders__ctor_m7B2ADBB9D3D1CCBF60F565A10813D46D95F50561,
	DestructionEffectLaser_Start_mEE40BDCFAA8606867D4DE10BE748DBC686967E23,
	DestructionEffectLaser_Update_m8E52C4FE4399EEB7E6151DFCAB4B134BD5C268D5,
	DestructionEffectLaser__ctor_mF75384A5D028D953963E438F45B0C54ED26B0D1F,
	MoveLaser1_Start_m65994D75F536382A7BCEC75AD2CBBFE6A0395EFC,
	MoveLaser1_Update_mB14CB11907F598D69B541F78D97EF9613CFF234B,
	MoveLaser1_OnCollisionEnter2D_mE7A09A80DDF61607DA9902FCAC475D17F8805371,
	MoveLaser1__ctor_m891E52378394329C7912E59536B4D5AED2E2C429,
	ShootLaser1_Start_mC9E08BDEDABA139120B21BA3A23B08E3490CDCFE,
	ShootLaser1_Update_mA57094A89B4E1DFCCBB74B3FDD62A63F8A38AD35,
	ShootLaser1_Shoot_m888912CA48BE2061D167B0665DEF96EE3A0D8AFA,
	ShootLaser1__ctor_mA41F457EBC289D3DB39825F1C7ADCC529437630E,
	MoveShip_Start_m633249C1BDACD931611617D01EF31FE21B804819,
	MoveShip_Update_mA926EB5714CE554E6AD2264E888E44D8756E30A2,
	MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749,
	MoveShip_OnCollisionEnter2D_m49C54471E2C7F1AD2780BECFCD186FA8E322CFEC,
	MoveShip_OnCollisionExit2D_m86C77026B656E4D502D73BC55F0E946A4F3F92EE,
	MoveShip__ctor_m6DDFD05FA781A523D7E648BAE8669F7836FD8ADA,
	PropulsionEffect_Start_m5B5C5CF51642E66D1DA10F9A8814B042D4F73BE4,
	PropulsionEffect_Update_m1F2A89E9B46D24D010D6F7A150193A4304A29145,
	PropulsionEffect__ctor_mA7C23DCB8BB410B81AE781CD1A4A56161ECE8FC5,
	ManageLifeBar_Start_m44A0AA415C799AC8D66D3BE10A703D781C93688F,
	ManageLifeBar_Update_mD0FBC92BCE43B59C8A5D87643662D894D00D2CEF,
	ManageLifeBar_AddToLife_m89DF759F658CB7AB76CD8E059C34E93D04E166E3,
	ManageLifeBar_RemoveToLife_mD92710734AE395928F87BFE55661FB8AA6A1368D,
	ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8,
	ManageLifeBar__ctor_m4F28E430C644E499708D8ABBF307B37230DB4B7B,
	ManagerScore_Update_mB614E9A52C210E11FF01CCE6CBCED218E7C0C9C9,
	ManagerScore_AddToScore_m0AC27C2FCF200C3CCCAA432A6B572495FE9F5775,
	ManagerScore_GetScore_mE526A8E73089A0AE6C1AB90B84D8363DF2CEA16C,
	ManagerScore__ctor_m2817EA953C04C9D3D29D0423A40954A5A410EC21,
};
static const int32_t s_InvokerIndices[171] = 
{
	1530,
	1530,
	1251,
	1251,
	1280,
	1280,
	1530,
	1530,
	1509,
	1509,
	1525,
	1509,
	1282,
	1509,
	1282,
	1473,
	1251,
	1507,
	1280,
	1507,
	1280,
	1530,
	1261,
	1261,
	331,
	1530,
	631,
	1261,
	1174,
	1530,
	1509,
	1282,
	1530,
	1261,
	1261,
	331,
	1530,
	1530,
	1530,
	1261,
	1261,
	1530,
	1509,
	1282,
	1251,
	1530,
	1261,
	1261,
	331,
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1261,
	1261,
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1179,
	1530,
	1530,
	1530,
	1530,
	1530,
	925,
	1473,
	1530,
	1530,
	1530,
	1530,
	1261,
	1530,
	1530,
	1530,
	1530,
	1280,
	1251,
	1251,
	1473,
	1251,
	1473,
	1530,
	1530,
	1530,
	1530,
	983,
	983,
	383,
	985,
	1530,
	2424,
	1530,
	531,
	1146,
	1146,
	1251,
	1530,
	1507,
	1485,
	1530,
	1485,
	1251,
	1530,
	1507,
	1485,
	1530,
	1485,
	1530,
	1530,
	1530,
	1530,
	1261,
	1530,
	1251,
	1530,
	1530,
	1509,
	1509,
	1509,
	1509,
	1509,
	1509,
	1091,
	1091,
	1091,
	1091,
	1091,
	1091,
	1091,
	1091,
	1091,
	1091,
	1146,
	1146,
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1261,
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1525,
	1261,
	1261,
	1530,
	1530,
	1530,
	1530,
	1530,
	1530,
	1251,
	1251,
	1473,
	1530,
	1530,
	1251,
	1473,
	1530,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	171,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
