﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Collision2D
struct Collision2D_t95B5FD331CE95276D3658140844190B485D26564;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// DestructionEffectEnemy
struct DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C;
// DestructionEffectLaser
struct DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// ManageAttributes
struct ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9;
// ManageEnemy
struct ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC;
// ManageGame
struct ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0;
// ManageLifeBar
struct ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57;
// ManagerScore
struct ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// MoveBackground
struct MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988;
// MoveLaser1
struct MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B;
// MoveMeteor
struct MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98;
// MoveShip
struct MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D;
// PropulsionEffect
struct PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17;
// System.Random
struct Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// ScrollBackground
struct ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951;
// ShootLaser1
struct ShootLaser1_t87558B0D9E149549A87216AB43D82F10C0AC0896;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Type
struct Type_t;
// ViewBorders
struct ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral19E30120BF148B4BD74A0C6CE7F85A461E68A628;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral2AD47C03F7A83F82E3B2ADFE8A60F1727FD3BEFD;
IL2CPP_EXTERN_C String_t* _stringLiteral491808D7EEB006360E8FF7C51AA2757B43D35E12;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m0F4FCA57A586D78D592E624FE089FC61DF99EF86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisDestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C_mD62FF1BD23C0599E9CFDC98E4ADE20C7937244B1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisDestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1_mE2328AB002D256692BFE0A89BB2140F39E760423_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisMoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A_m9535F8179E6E9064B77556325A1AED291369EB53_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisPolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D_m734FCF5A42A89193927B270F69F5A1CBCA00FD82_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m153808182EE4702E05177827BB9D2D3961116B24_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var;
struct ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 ;

struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.Random
struct  Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_3;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_4;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___SeedArray_5;

public:
	inline static int32_t get_offset_of_inext_3() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___inext_3)); }
	inline int32_t get_inext_3() const { return ___inext_3; }
	inline int32_t* get_address_of_inext_3() { return &___inext_3; }
	inline void set_inext_3(int32_t value)
	{
		___inext_3 = value;
	}

	inline static int32_t get_offset_of_inextp_4() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___inextp_4)); }
	inline int32_t get_inextp_4() const { return ___inextp_4; }
	inline int32_t* get_address_of_inextp_4() { return &___inextp_4; }
	inline void set_inextp_4(int32_t value)
	{
		___inextp_4 = value;
	}

	inline static int32_t get_offset_of_SeedArray_5() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___SeedArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_SeedArray_5() const { return ___SeedArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_SeedArray_5() { return &___SeedArray_5; }
	inline void set_SeedArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___SeedArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SeedArray_5), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___list_0)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_list_0() const { return ___list_0; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Bounds
struct  Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// UnityEngine.Collision2D
struct  Collision2D_t95B5FD331CE95276D3658140844190B485D26564  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_RelativeVelocity_4;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_5;
	// System.Int32 UnityEngine.Collision2D::m_ContactCount
	int32_t ___m_ContactCount_6;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_ReusedContacts
	ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277* ___m_ReusedContacts_7;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_LegacyContacts
	ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277* ___m_LegacyContacts_8;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_Collider_0)); }
	inline int32_t get_m_Collider_0() const { return ___m_Collider_0; }
	inline int32_t* get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(int32_t value)
	{
		___m_Collider_0 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_1() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_OtherCollider_1)); }
	inline int32_t get_m_OtherCollider_1() const { return ___m_OtherCollider_1; }
	inline int32_t* get_address_of_m_OtherCollider_1() { return &___m_OtherCollider_1; }
	inline void set_m_OtherCollider_1(int32_t value)
	{
		___m_OtherCollider_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_Rigidbody_2)); }
	inline int32_t get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline int32_t* get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(int32_t value)
	{
		___m_Rigidbody_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_3() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_OtherRigidbody_3)); }
	inline int32_t get_m_OtherRigidbody_3() const { return ___m_OtherRigidbody_3; }
	inline int32_t* get_address_of_m_OtherRigidbody_3() { return &___m_OtherRigidbody_3; }
	inline void set_m_OtherRigidbody_3(int32_t value)
	{
		___m_OtherRigidbody_3 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_4() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_RelativeVelocity_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_RelativeVelocity_4() const { return ___m_RelativeVelocity_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_RelativeVelocity_4() { return &___m_RelativeVelocity_4; }
	inline void set_m_RelativeVelocity_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_RelativeVelocity_4 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_5() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_Enabled_5)); }
	inline int32_t get_m_Enabled_5() const { return ___m_Enabled_5; }
	inline int32_t* get_address_of_m_Enabled_5() { return &___m_Enabled_5; }
	inline void set_m_Enabled_5(int32_t value)
	{
		___m_Enabled_5 = value;
	}

	inline static int32_t get_offset_of_m_ContactCount_6() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_ContactCount_6)); }
	inline int32_t get_m_ContactCount_6() const { return ___m_ContactCount_6; }
	inline int32_t* get_address_of_m_ContactCount_6() { return &___m_ContactCount_6; }
	inline void set_m_ContactCount_6(int32_t value)
	{
		___m_ContactCount_6 = value;
	}

	inline static int32_t get_offset_of_m_ReusedContacts_7() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_ReusedContacts_7)); }
	inline ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277* get_m_ReusedContacts_7() const { return ___m_ReusedContacts_7; }
	inline ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277** get_address_of_m_ReusedContacts_7() { return &___m_ReusedContacts_7; }
	inline void set_m_ReusedContacts_7(ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277* value)
	{
		___m_ReusedContacts_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReusedContacts_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_LegacyContacts_8() { return static_cast<int32_t>(offsetof(Collision2D_t95B5FD331CE95276D3658140844190B485D26564, ___m_LegacyContacts_8)); }
	inline ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277* get_m_LegacyContacts_8() const { return ___m_LegacyContacts_8; }
	inline ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277** get_address_of_m_LegacyContacts_8() { return &___m_LegacyContacts_8; }
	inline void set_m_LegacyContacts_8(ContactPoint2DU5BU5D_t7AE0F95E9BFC90DE859575689AA76B503D433277* value)
	{
		___m_LegacyContacts_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LegacyContacts_8), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t95B5FD331CE95276D3658140844190B485D26564_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 * ___m_ReusedContacts_7;
	ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 * ___m_LegacyContacts_8;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t95B5FD331CE95276D3658140844190B485D26564_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_RelativeVelocity_4;
	int32_t ___m_Enabled_5;
	int32_t ___m_ContactCount_6;
	ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 * ___m_ReusedContacts_7;
	ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 * ___m_LegacyContacts_8;
};

// UnityEngine.ContactPoint2D
struct  ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 
{
public:
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Point
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Point_0;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_Normal
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Normal_1;
	// UnityEngine.Vector2 UnityEngine.ContactPoint2D::m_RelativeVelocity
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_RelativeVelocity_2;
	// System.Single UnityEngine.ContactPoint2D::m_Separation
	float ___m_Separation_3;
	// System.Single UnityEngine.ContactPoint2D::m_NormalImpulse
	float ___m_NormalImpulse_4;
	// System.Single UnityEngine.ContactPoint2D::m_TangentImpulse
	float ___m_TangentImpulse_5;
	// System.Int32 UnityEngine.ContactPoint2D::m_Collider
	int32_t ___m_Collider_6;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherCollider
	int32_t ___m_OtherCollider_7;
	// System.Int32 UnityEngine.ContactPoint2D::m_Rigidbody
	int32_t ___m_Rigidbody_8;
	// System.Int32 UnityEngine.ContactPoint2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_9;
	// System.Int32 UnityEngine.ContactPoint2D::m_Enabled
	int32_t ___m_Enabled_10;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_Point_0)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_Normal_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_2() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_RelativeVelocity_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_RelativeVelocity_2() const { return ___m_RelativeVelocity_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_RelativeVelocity_2() { return &___m_RelativeVelocity_2; }
	inline void set_m_RelativeVelocity_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_RelativeVelocity_2 = value;
	}

	inline static int32_t get_offset_of_m_Separation_3() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_Separation_3)); }
	inline float get_m_Separation_3() const { return ___m_Separation_3; }
	inline float* get_address_of_m_Separation_3() { return &___m_Separation_3; }
	inline void set_m_Separation_3(float value)
	{
		___m_Separation_3 = value;
	}

	inline static int32_t get_offset_of_m_NormalImpulse_4() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_NormalImpulse_4)); }
	inline float get_m_NormalImpulse_4() const { return ___m_NormalImpulse_4; }
	inline float* get_address_of_m_NormalImpulse_4() { return &___m_NormalImpulse_4; }
	inline void set_m_NormalImpulse_4(float value)
	{
		___m_NormalImpulse_4 = value;
	}

	inline static int32_t get_offset_of_m_TangentImpulse_5() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_TangentImpulse_5)); }
	inline float get_m_TangentImpulse_5() const { return ___m_TangentImpulse_5; }
	inline float* get_address_of_m_TangentImpulse_5() { return &___m_TangentImpulse_5; }
	inline void set_m_TangentImpulse_5(float value)
	{
		___m_TangentImpulse_5 = value;
	}

	inline static int32_t get_offset_of_m_Collider_6() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_Collider_6)); }
	inline int32_t get_m_Collider_6() const { return ___m_Collider_6; }
	inline int32_t* get_address_of_m_Collider_6() { return &___m_Collider_6; }
	inline void set_m_Collider_6(int32_t value)
	{
		___m_Collider_6 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_7() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_OtherCollider_7)); }
	inline int32_t get_m_OtherCollider_7() const { return ___m_OtherCollider_7; }
	inline int32_t* get_address_of_m_OtherCollider_7() { return &___m_OtherCollider_7; }
	inline void set_m_OtherCollider_7(int32_t value)
	{
		___m_OtherCollider_7 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_8() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_Rigidbody_8)); }
	inline int32_t get_m_Rigidbody_8() const { return ___m_Rigidbody_8; }
	inline int32_t* get_address_of_m_Rigidbody_8() { return &___m_Rigidbody_8; }
	inline void set_m_Rigidbody_8(int32_t value)
	{
		___m_Rigidbody_8 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_9() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_OtherRigidbody_9)); }
	inline int32_t get_m_OtherRigidbody_9() const { return ___m_OtherRigidbody_9; }
	inline int32_t* get_address_of_m_OtherRigidbody_9() { return &___m_OtherRigidbody_9; }
	inline void set_m_OtherRigidbody_9(int32_t value)
	{
		___m_OtherRigidbody_9 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_10() { return static_cast<int32_t>(offsetof(ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62, ___m_Enabled_10)); }
	inline int32_t get_m_Enabled_10() const { return ___m_Enabled_10; }
	inline int32_t* get_address_of_m_Enabled_10() { return &___m_Enabled_10; }
	inline void set_m_Enabled_10(int32_t value)
	{
		___m_Enabled_10 = value;
	}
};


// UnityEngine.KeyCode
struct  KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Sprite
struct  Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Collider2D
struct  Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// DestructionEffectEnemy
struct  DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Color DestructionEffectEnemy::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_4;

public:
	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C, ___color_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_4() const { return ___color_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_4 = value;
	}
};


// DestructionEffectLaser
struct  DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<System.Int32> DestructionEffectLaser::childrensIndex
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___childrensIndex_4;
	// UnityEngine.Color DestructionEffectLaser::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_5;

public:
	inline static int32_t get_offset_of_childrensIndex_4() { return static_cast<int32_t>(offsetof(DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1, ___childrensIndex_4)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_childrensIndex_4() const { return ___childrensIndex_4; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_childrensIndex_4() { return &___childrensIndex_4; }
	inline void set_childrensIndex_4(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___childrensIndex_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___childrensIndex_4), (void*)value);
	}

	inline static int32_t get_offset_of_color_5() { return static_cast<int32_t>(offsetof(DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1, ___color_5)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_5() const { return ___color_5; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_5() { return &___color_5; }
	inline void set_color_5(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_5 = value;
	}
};


// ManageAttributes
struct  ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 ManageAttributes::pointsToWin
	int32_t ___pointsToWin_4;
	// System.Int32 ManageAttributes::attackPoints
	int32_t ___attackPoints_5;
	// System.Int32 ManageAttributes::lifePoints
	int32_t ___lifePoints_6;
	// System.Boolean ManageAttributes::belongToThePlayer
	bool ___belongToThePlayer_7;
	// System.Int32 ManageAttributes::restOfLifePoints
	int32_t ___restOfLifePoints_8;

public:
	inline static int32_t get_offset_of_pointsToWin_4() { return static_cast<int32_t>(offsetof(ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9, ___pointsToWin_4)); }
	inline int32_t get_pointsToWin_4() const { return ___pointsToWin_4; }
	inline int32_t* get_address_of_pointsToWin_4() { return &___pointsToWin_4; }
	inline void set_pointsToWin_4(int32_t value)
	{
		___pointsToWin_4 = value;
	}

	inline static int32_t get_offset_of_attackPoints_5() { return static_cast<int32_t>(offsetof(ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9, ___attackPoints_5)); }
	inline int32_t get_attackPoints_5() const { return ___attackPoints_5; }
	inline int32_t* get_address_of_attackPoints_5() { return &___attackPoints_5; }
	inline void set_attackPoints_5(int32_t value)
	{
		___attackPoints_5 = value;
	}

	inline static int32_t get_offset_of_lifePoints_6() { return static_cast<int32_t>(offsetof(ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9, ___lifePoints_6)); }
	inline int32_t get_lifePoints_6() const { return ___lifePoints_6; }
	inline int32_t* get_address_of_lifePoints_6() { return &___lifePoints_6; }
	inline void set_lifePoints_6(int32_t value)
	{
		___lifePoints_6 = value;
	}

	inline static int32_t get_offset_of_belongToThePlayer_7() { return static_cast<int32_t>(offsetof(ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9, ___belongToThePlayer_7)); }
	inline bool get_belongToThePlayer_7() const { return ___belongToThePlayer_7; }
	inline bool* get_address_of_belongToThePlayer_7() { return &___belongToThePlayer_7; }
	inline void set_belongToThePlayer_7(bool value)
	{
		___belongToThePlayer_7 = value;
	}

	inline static int32_t get_offset_of_restOfLifePoints_8() { return static_cast<int32_t>(offsetof(ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9, ___restOfLifePoints_8)); }
	inline int32_t get_restOfLifePoints_8() const { return ___restOfLifePoints_8; }
	inline int32_t* get_address_of_restOfLifePoints_8() { return &___restOfLifePoints_8; }
	inline void set_restOfLifePoints_8(int32_t value)
	{
		___restOfLifePoints_8 = value;
	}
};


// ManageEnemy
struct  ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 ManageEnemy::minNumberOfMeteor
	int32_t ___minNumberOfMeteor_4;
	// System.Int32 ManageEnemy::maxNumberOfMeteor
	int32_t ___maxNumberOfMeteor_5;
	// UnityEngine.GameObject ManageEnemy::prefabEnnemy
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___prefabEnnemy_6;
	// System.Random ManageEnemy::generator
	Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * ___generator_7;
	// UnityEngine.Vector3 ManageEnemy::viewBorderLeftTop
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___viewBorderLeftTop_8;
	// UnityEngine.Vector3 ManageEnemy::viewBorderRightTop
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___viewBorderRightTop_9;

public:
	inline static int32_t get_offset_of_minNumberOfMeteor_4() { return static_cast<int32_t>(offsetof(ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC, ___minNumberOfMeteor_4)); }
	inline int32_t get_minNumberOfMeteor_4() const { return ___minNumberOfMeteor_4; }
	inline int32_t* get_address_of_minNumberOfMeteor_4() { return &___minNumberOfMeteor_4; }
	inline void set_minNumberOfMeteor_4(int32_t value)
	{
		___minNumberOfMeteor_4 = value;
	}

	inline static int32_t get_offset_of_maxNumberOfMeteor_5() { return static_cast<int32_t>(offsetof(ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC, ___maxNumberOfMeteor_5)); }
	inline int32_t get_maxNumberOfMeteor_5() const { return ___maxNumberOfMeteor_5; }
	inline int32_t* get_address_of_maxNumberOfMeteor_5() { return &___maxNumberOfMeteor_5; }
	inline void set_maxNumberOfMeteor_5(int32_t value)
	{
		___maxNumberOfMeteor_5 = value;
	}

	inline static int32_t get_offset_of_prefabEnnemy_6() { return static_cast<int32_t>(offsetof(ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC, ___prefabEnnemy_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_prefabEnnemy_6() const { return ___prefabEnnemy_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_prefabEnnemy_6() { return &___prefabEnnemy_6; }
	inline void set_prefabEnnemy_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___prefabEnnemy_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabEnnemy_6), (void*)value);
	}

	inline static int32_t get_offset_of_generator_7() { return static_cast<int32_t>(offsetof(ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC, ___generator_7)); }
	inline Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * get_generator_7() const { return ___generator_7; }
	inline Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 ** get_address_of_generator_7() { return &___generator_7; }
	inline void set_generator_7(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * value)
	{
		___generator_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___generator_7), (void*)value);
	}

	inline static int32_t get_offset_of_viewBorderLeftTop_8() { return static_cast<int32_t>(offsetof(ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC, ___viewBorderLeftTop_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_viewBorderLeftTop_8() const { return ___viewBorderLeftTop_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_viewBorderLeftTop_8() { return &___viewBorderLeftTop_8; }
	inline void set_viewBorderLeftTop_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___viewBorderLeftTop_8 = value;
	}

	inline static int32_t get_offset_of_viewBorderRightTop_9() { return static_cast<int32_t>(offsetof(ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC, ___viewBorderRightTop_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_viewBorderRightTop_9() const { return ___viewBorderRightTop_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_viewBorderRightTop_9() { return &___viewBorderRightTop_9; }
	inline void set_viewBorderRightTop_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___viewBorderRightTop_9 = value;
	}
};


// ManageGame
struct  ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ManageGame::gamePauseMenu
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gamePauseMenu_5;
	// System.Boolean ManageGame::isPaused
	bool ___isPaused_6;

public:
	inline static int32_t get_offset_of_gamePauseMenu_5() { return static_cast<int32_t>(offsetof(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0, ___gamePauseMenu_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gamePauseMenu_5() const { return ___gamePauseMenu_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gamePauseMenu_5() { return &___gamePauseMenu_5; }
	inline void set_gamePauseMenu_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gamePauseMenu_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gamePauseMenu_5), (void*)value);
	}

	inline static int32_t get_offset_of_isPaused_6() { return static_cast<int32_t>(offsetof(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0, ___isPaused_6)); }
	inline bool get_isPaused_6() const { return ___isPaused_6; }
	inline bool* get_address_of_isPaused_6() { return &___isPaused_6; }
	inline void set_isPaused_6(bool value)
	{
		___isPaused_6 = value;
	}
};

struct ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_StaticFields
{
public:
	// ManageGame ManageGame::Instance
	ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_StaticFields, ___Instance_4)); }
	inline ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * get_Instance_4() const { return ___Instance_4; }
	inline ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// ManageLifeBar
struct  ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 ManageLifeBar::initLife
	int32_t ___initLife_5;
	// System.String ManageLifeBar::newTag
	String_t* ___newTag_6;
	// UnityEngine.GameObject[] ManageLifeBar::prefabNumbers
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___prefabNumbers_7;
	// System.Single ManageLifeBar::offsetX
	float ___offsetX_8;
	// System.Single ManageLifeBar::space
	float ___space_9;
	// System.Int32 ManageLifeBar::currentLife
	int32_t ___currentLife_10;

public:
	inline static int32_t get_offset_of_initLife_5() { return static_cast<int32_t>(offsetof(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57, ___initLife_5)); }
	inline int32_t get_initLife_5() const { return ___initLife_5; }
	inline int32_t* get_address_of_initLife_5() { return &___initLife_5; }
	inline void set_initLife_5(int32_t value)
	{
		___initLife_5 = value;
	}

	inline static int32_t get_offset_of_newTag_6() { return static_cast<int32_t>(offsetof(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57, ___newTag_6)); }
	inline String_t* get_newTag_6() const { return ___newTag_6; }
	inline String_t** get_address_of_newTag_6() { return &___newTag_6; }
	inline void set_newTag_6(String_t* value)
	{
		___newTag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newTag_6), (void*)value);
	}

	inline static int32_t get_offset_of_prefabNumbers_7() { return static_cast<int32_t>(offsetof(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57, ___prefabNumbers_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_prefabNumbers_7() const { return ___prefabNumbers_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_prefabNumbers_7() { return &___prefabNumbers_7; }
	inline void set_prefabNumbers_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___prefabNumbers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabNumbers_7), (void*)value);
	}

	inline static int32_t get_offset_of_offsetX_8() { return static_cast<int32_t>(offsetof(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57, ___offsetX_8)); }
	inline float get_offsetX_8() const { return ___offsetX_8; }
	inline float* get_address_of_offsetX_8() { return &___offsetX_8; }
	inline void set_offsetX_8(float value)
	{
		___offsetX_8 = value;
	}

	inline static int32_t get_offset_of_space_9() { return static_cast<int32_t>(offsetof(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57, ___space_9)); }
	inline float get_space_9() const { return ___space_9; }
	inline float* get_address_of_space_9() { return &___space_9; }
	inline void set_space_9(float value)
	{
		___space_9 = value;
	}

	inline static int32_t get_offset_of_currentLife_10() { return static_cast<int32_t>(offsetof(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57, ___currentLife_10)); }
	inline int32_t get_currentLife_10() const { return ___currentLife_10; }
	inline int32_t* get_address_of_currentLife_10() { return &___currentLife_10; }
	inline void set_currentLife_10(int32_t value)
	{
		___currentLife_10 = value;
	}
};

struct ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields
{
public:
	// ManageLifeBar ManageLifeBar::Instance
	ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields, ___Instance_4)); }
	inline ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * get_Instance_4() const { return ___Instance_4; }
	inline ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// ManagerScore
struct  ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 ManagerScore::initScore
	int32_t ___initScore_5;
	// System.String ManagerScore::newTag
	String_t* ___newTag_6;
	// UnityEngine.GameObject[] ManagerScore::prefabNumbers
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___prefabNumbers_7;
	// System.Single ManagerScore::space
	float ___space_8;
	// System.Int32 ManagerScore::score
	int32_t ___score_9;

public:
	inline static int32_t get_offset_of_initScore_5() { return static_cast<int32_t>(offsetof(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695, ___initScore_5)); }
	inline int32_t get_initScore_5() const { return ___initScore_5; }
	inline int32_t* get_address_of_initScore_5() { return &___initScore_5; }
	inline void set_initScore_5(int32_t value)
	{
		___initScore_5 = value;
	}

	inline static int32_t get_offset_of_newTag_6() { return static_cast<int32_t>(offsetof(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695, ___newTag_6)); }
	inline String_t* get_newTag_6() const { return ___newTag_6; }
	inline String_t** get_address_of_newTag_6() { return &___newTag_6; }
	inline void set_newTag_6(String_t* value)
	{
		___newTag_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___newTag_6), (void*)value);
	}

	inline static int32_t get_offset_of_prefabNumbers_7() { return static_cast<int32_t>(offsetof(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695, ___prefabNumbers_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_prefabNumbers_7() const { return ___prefabNumbers_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_prefabNumbers_7() { return &___prefabNumbers_7; }
	inline void set_prefabNumbers_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___prefabNumbers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefabNumbers_7), (void*)value);
	}

	inline static int32_t get_offset_of_space_8() { return static_cast<int32_t>(offsetof(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695, ___space_8)); }
	inline float get_space_8() const { return ___space_8; }
	inline float* get_address_of_space_8() { return &___space_8; }
	inline void set_space_8(float value)
	{
		___space_8 = value;
	}

	inline static int32_t get_offset_of_score_9() { return static_cast<int32_t>(offsetof(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695, ___score_9)); }
	inline int32_t get_score_9() const { return ___score_9; }
	inline int32_t* get_address_of_score_9() { return &___score_9; }
	inline void set_score_9(int32_t value)
	{
		___score_9 = value;
	}
};

struct ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_StaticFields
{
public:
	// ManagerScore ManagerScore::Instance
	ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_StaticFields, ___Instance_4)); }
	inline ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * get_Instance_4() const { return ___Instance_4; }
	inline ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Instance_4), (void*)value);
	}
};


// MoveBackground
struct  MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject MoveBackground::background1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___background1_4;
	// UnityEngine.GameObject MoveBackground::background2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___background2_5;
	// UnityEngine.GameObject MoveBackground::background3
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___background3_6;
	// System.Single MoveBackground::offsetX
	float ___offsetX_7;
	// UnityEngine.Vector2 MoveBackground::speed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___speed_8;
	// System.Boolean MoveBackground::toRight
	bool ___toRight_9;
	// ViewBorders MoveBackground::viewBorders
	ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * ___viewBorders_10;
	// System.Single MoveBackground::sizeY
	float ___sizeY_11;

public:
	inline static int32_t get_offset_of_background1_4() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___background1_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_background1_4() const { return ___background1_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_background1_4() { return &___background1_4; }
	inline void set_background1_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___background1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background1_4), (void*)value);
	}

	inline static int32_t get_offset_of_background2_5() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___background2_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_background2_5() const { return ___background2_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_background2_5() { return &___background2_5; }
	inline void set_background2_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___background2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background2_5), (void*)value);
	}

	inline static int32_t get_offset_of_background3_6() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___background3_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_background3_6() const { return ___background3_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_background3_6() { return &___background3_6; }
	inline void set_background3_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___background3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background3_6), (void*)value);
	}

	inline static int32_t get_offset_of_offsetX_7() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___offsetX_7)); }
	inline float get_offsetX_7() const { return ___offsetX_7; }
	inline float* get_address_of_offsetX_7() { return &___offsetX_7; }
	inline void set_offsetX_7(float value)
	{
		___offsetX_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___speed_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_speed_8() const { return ___speed_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_toRight_9() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___toRight_9)); }
	inline bool get_toRight_9() const { return ___toRight_9; }
	inline bool* get_address_of_toRight_9() { return &___toRight_9; }
	inline void set_toRight_9(bool value)
	{
		___toRight_9 = value;
	}

	inline static int32_t get_offset_of_viewBorders_10() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___viewBorders_10)); }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * get_viewBorders_10() const { return ___viewBorders_10; }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C ** get_address_of_viewBorders_10() { return &___viewBorders_10; }
	inline void set_viewBorders_10(ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * value)
	{
		___viewBorders_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewBorders_10), (void*)value);
	}

	inline static int32_t get_offset_of_sizeY_11() { return static_cast<int32_t>(offsetof(MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988, ___sizeY_11)); }
	inline float get_sizeY_11() const { return ___sizeY_11; }
	inline float* get_address_of_sizeY_11() { return &___sizeY_11; }
	inline void set_sizeY_11(float value)
	{
		___sizeY_11 = value;
	}
};


// MoveLaser1
struct  MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single MoveLaser1::speed
	float ___speed_4;
	// ViewBorders MoveLaser1::viewBorders
	ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * ___viewBorders_5;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_viewBorders_5() { return static_cast<int32_t>(offsetof(MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B, ___viewBorders_5)); }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * get_viewBorders_5() const { return ___viewBorders_5; }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C ** get_address_of_viewBorders_5() { return &___viewBorders_5; }
	inline void set_viewBorders_5(ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * value)
	{
		___viewBorders_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewBorders_5), (void*)value);
	}
};


// MoveMeteor
struct  MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single MoveMeteor::speedMaxY
	float ___speedMaxY_4;
	// ViewBorders MoveMeteor::viewBorders
	ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * ___viewBorders_5;
	// UnityEngine.Vector3 MoveMeteor::size
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___size_6;

public:
	inline static int32_t get_offset_of_speedMaxY_4() { return static_cast<int32_t>(offsetof(MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98, ___speedMaxY_4)); }
	inline float get_speedMaxY_4() const { return ___speedMaxY_4; }
	inline float* get_address_of_speedMaxY_4() { return &___speedMaxY_4; }
	inline void set_speedMaxY_4(float value)
	{
		___speedMaxY_4 = value;
	}

	inline static int32_t get_offset_of_viewBorders_5() { return static_cast<int32_t>(offsetof(MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98, ___viewBorders_5)); }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * get_viewBorders_5() const { return ___viewBorders_5; }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C ** get_address_of_viewBorders_5() { return &___viewBorders_5; }
	inline void set_viewBorders_5(ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * value)
	{
		___viewBorders_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewBorders_5), (void*)value);
	}

	inline static int32_t get_offset_of_size_6() { return static_cast<int32_t>(offsetof(MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98, ___size_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_size_6() const { return ___size_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_size_6() { return &___size_6; }
	inline void set_size_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___size_6 = value;
	}
};


// MoveShip
struct  MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single MoveShip::speed
	float ___speed_4;
	// ViewBorders MoveShip::viewBorders
	ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * ___viewBorders_5;
	// UnityEngine.Vector2 MoveShip::input
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___input_6;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_viewBorders_5() { return static_cast<int32_t>(offsetof(MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A, ___viewBorders_5)); }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * get_viewBorders_5() const { return ___viewBorders_5; }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C ** get_address_of_viewBorders_5() { return &___viewBorders_5; }
	inline void set_viewBorders_5(ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * value)
	{
		___viewBorders_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewBorders_5), (void*)value);
	}

	inline static int32_t get_offset_of_input_6() { return static_cast<int32_t>(offsetof(MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A, ___input_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_input_6() const { return ___input_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_input_6() { return &___input_6; }
	inline void set_input_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___input_6 = value;
	}
};


// UnityEngine.PolygonCollider2D
struct  PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D  : public Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722
{
public:

public:
};


// PropulsionEffect
struct  PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Sprite PropulsionEffect::spriteSpeedMin
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___spriteSpeedMin_4;
	// UnityEngine.Sprite PropulsionEffect::spriteSpeedMedium
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___spriteSpeedMedium_5;
	// UnityEngine.Sprite PropulsionEffect::spriteSpeedMax
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___spriteSpeedMax_6;
	// UnityEngine.Vector3 PropulsionEffect::scale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale_7;
	// UnityEngine.GameObject PropulsionEffect::fire
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___fire_8;

public:
	inline static int32_t get_offset_of_spriteSpeedMin_4() { return static_cast<int32_t>(offsetof(PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17, ___spriteSpeedMin_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_spriteSpeedMin_4() const { return ___spriteSpeedMin_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_spriteSpeedMin_4() { return &___spriteSpeedMin_4; }
	inline void set_spriteSpeedMin_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___spriteSpeedMin_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteSpeedMin_4), (void*)value);
	}

	inline static int32_t get_offset_of_spriteSpeedMedium_5() { return static_cast<int32_t>(offsetof(PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17, ___spriteSpeedMedium_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_spriteSpeedMedium_5() const { return ___spriteSpeedMedium_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_spriteSpeedMedium_5() { return &___spriteSpeedMedium_5; }
	inline void set_spriteSpeedMedium_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___spriteSpeedMedium_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteSpeedMedium_5), (void*)value);
	}

	inline static int32_t get_offset_of_spriteSpeedMax_6() { return static_cast<int32_t>(offsetof(PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17, ___spriteSpeedMax_6)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_spriteSpeedMax_6() const { return ___spriteSpeedMax_6; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_spriteSpeedMax_6() { return &___spriteSpeedMax_6; }
	inline void set_spriteSpeedMax_6(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___spriteSpeedMax_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteSpeedMax_6), (void*)value);
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17, ___scale_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_scale_7() const { return ___scale_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_fire_8() { return static_cast<int32_t>(offsetof(PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17, ___fire_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_fire_8() const { return ___fire_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_fire_8() { return &___fire_8; }
	inline void set_fire_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___fire_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fire_8), (void*)value);
	}
};


// ScrollBackground
struct  ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ScrollBackground::background1
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___background1_4;
	// UnityEngine.GameObject ScrollBackground::background2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___background2_5;
	// UnityEngine.GameObject ScrollBackground::background3
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___background3_6;
	// System.Single ScrollBackground::speedY
	float ___speedY_7;
	// ViewBorders ScrollBackground::viewBorders
	ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * ___viewBorders_8;
	// System.Single ScrollBackground::sizeY
	float ___sizeY_9;

public:
	inline static int32_t get_offset_of_background1_4() { return static_cast<int32_t>(offsetof(ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951, ___background1_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_background1_4() const { return ___background1_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_background1_4() { return &___background1_4; }
	inline void set_background1_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___background1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background1_4), (void*)value);
	}

	inline static int32_t get_offset_of_background2_5() { return static_cast<int32_t>(offsetof(ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951, ___background2_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_background2_5() const { return ___background2_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_background2_5() { return &___background2_5; }
	inline void set_background2_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___background2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background2_5), (void*)value);
	}

	inline static int32_t get_offset_of_background3_6() { return static_cast<int32_t>(offsetof(ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951, ___background3_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_background3_6() const { return ___background3_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_background3_6() { return &___background3_6; }
	inline void set_background3_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___background3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background3_6), (void*)value);
	}

	inline static int32_t get_offset_of_speedY_7() { return static_cast<int32_t>(offsetof(ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951, ___speedY_7)); }
	inline float get_speedY_7() const { return ___speedY_7; }
	inline float* get_address_of_speedY_7() { return &___speedY_7; }
	inline void set_speedY_7(float value)
	{
		___speedY_7 = value;
	}

	inline static int32_t get_offset_of_viewBorders_8() { return static_cast<int32_t>(offsetof(ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951, ___viewBorders_8)); }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * get_viewBorders_8() const { return ___viewBorders_8; }
	inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C ** get_address_of_viewBorders_8() { return &___viewBorders_8; }
	inline void set_viewBorders_8(ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * value)
	{
		___viewBorders_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___viewBorders_8), (void*)value);
	}

	inline static int32_t get_offset_of_sizeY_9() { return static_cast<int32_t>(offsetof(ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951, ___sizeY_9)); }
	inline float get_sizeY_9() const { return ___sizeY_9; }
	inline float* get_address_of_sizeY_9() { return &___sizeY_9; }
	inline void set_sizeY_9(float value)
	{
		___sizeY_9 = value;
	}
};


// ShootLaser1
struct  ShootLaser1_t87558B0D9E149549A87216AB43D82F10C0AC0896  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ShootLaser1::laser
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___laser_4;
	// System.Boolean ShootLaser1::fromPlayer
	bool ___fromPlayer_5;

public:
	inline static int32_t get_offset_of_laser_4() { return static_cast<int32_t>(offsetof(ShootLaser1_t87558B0D9E149549A87216AB43D82F10C0AC0896, ___laser_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_laser_4() const { return ___laser_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_laser_4() { return &___laser_4; }
	inline void set_laser_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___laser_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___laser_4), (void*)value);
	}

	inline static int32_t get_offset_of_fromPlayer_5() { return static_cast<int32_t>(offsetof(ShootLaser1_t87558B0D9E149549A87216AB43D82F10C0AC0896, ___fromPlayer_5)); }
	inline bool get_fromPlayer_5() const { return ___fromPlayer_5; }
	inline bool* get_address_of_fromPlayer_5() { return &___fromPlayer_5; }
	inline void set_fromPlayer_5(bool value)
	{
		___fromPlayer_5 = value;
	}
};


// ViewBorders
struct  ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 ViewBorders::cameraBottomLeft
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cameraBottomLeft_4;
	// UnityEngine.Vector3 ViewBorders::cameraTopRight
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cameraTopRight_5;

public:
	inline static int32_t get_offset_of_cameraBottomLeft_4() { return static_cast<int32_t>(offsetof(ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C, ___cameraBottomLeft_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cameraBottomLeft_4() const { return ___cameraBottomLeft_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cameraBottomLeft_4() { return &___cameraBottomLeft_4; }
	inline void set_cameraBottomLeft_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cameraBottomLeft_4 = value;
	}

	inline static int32_t get_offset_of_cameraTopRight_5() { return static_cast<int32_t>(offsetof(ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C, ___cameraTopRight_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cameraTopRight_5() const { return ___cameraTopRight_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cameraTopRight_5() { return &___cameraTopRight_5; }
	inline void set_cameraTopRight_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cameraTopRight_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  List_1_GetEnumerator_m153808182EE4702E05177827BB9D2D3961116B24_gshared (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_gshared (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m0F4FCA57A586D78D592E624FE089FC61DF99EF86_gshared (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared (RuntimeObject * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Transform>()
inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
inline void List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_gshared)(__this, ___item0, method);
}
// System.Int32 UnityEngine.Transform::get_childCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
inline int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, int32_t, const RuntimeMethod*))List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline)(__this, ___index0, method);
}
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  List_1_GetEnumerator_m153808182EE4702E05177827BB9D2D3961116B24 (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))List_1_GetEnumerator_m153808182EE4702E05177827BB9D2D3961116B24_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
inline int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *, const RuntimeMethod*))Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
inline bool Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6 (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *, const RuntimeMethod*))Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
inline void Enumerator_Dispose_m0F4FCA57A586D78D592E624FE089FC61DF99EF86 (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *, const RuntimeMethod*))Enumerator_Dispose_m0F4FCA57A586D78D592E624FE089FC61DF99EF86_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<ManageAttributes>()
inline ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void System.Random::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Random__ctor_mF40AD1812BABC06235B661CCE513E4F74EEE9F05 (Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * __this, const RuntimeMethod* method);
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Camera_ViewportToWorldPoint_m1273EE3868551C6FF551ABA9A76DC7D66E883116 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, const RuntimeMethod* method);
// System.String UnityEngine.GameObject::get_tag()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186 (String_t* ___tag0, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097 (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 ManageEnemy::RandomPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ManageEnemy_RandomPosition_mAB1D66F48604EEBFD52F246843DD5DAE7E62086D (ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___size0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___target0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// System.Void ManageGame::ContinueGameLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_ContinueGameLevel_m9371DB8D650C9A6372522C0E1F99FF24413E3A99 (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method);
// System.Void ManageGame::PauseGameLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4 (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Time::set_timeScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA (float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.String System.Char::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8 (Il2CppChar* __this, const RuntimeMethod* method);
// System.Int32 System.Int32::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C (String_t* ___s0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::set_tag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_set_tag_m0EBA46574304C71E047A33BDD5F5D49E9D9A25BE (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<ViewBorders>()
inline ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Single ViewBorders::borderBottom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOut(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOut_m0A983F94132A2262E813A7FD17DDC6B666A5AC3C (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Collision2D_get_gameObject_m6F07B9CA1FAD187933EB6D8E44BD9F870658F89F (Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.ContactPoint2D UnityEngine.Collision2D::GetContact(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62  Collision2D_GetContact_m485AB3C5BE25E6CA83E7B7DA9524DBF9A94F4A77 (Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * __this, int32_t ___index0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.ContactPoint2D::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ContactPoint2D_get_point_mB8EB8CD492D607A7E61553DEE618C70DB6787DEF (ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<DestructionEffectLaser>()
inline DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1 * GameObject_AddComponent_TisDestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1_mE2328AB002D256692BFE0A89BB2140F39E760423 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Int32 ManageAttributes::AttackedBy(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9 (ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___other0, const RuntimeMethod* method);
// System.Void ManagerScore::AddToScore(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerScore_AddToScore_m0AC27C2FCF200C3CCCAA432A6B572495FE9F5775 (ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * __this, int32_t ___toAdd0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.PolygonCollider2D>()
inline PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D * GameObject_GetComponent_TisPolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D_m734FCF5A42A89193927B270F69F5A1CBCA00FD82 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<DestructionEffectEnemy>()
inline DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C * GameObject_AddComponent_TisDestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C_mD62FF1BD23C0599E9CFDC98E4ADE20C7937244B1 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Single ViewBorders::borderRight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method);
// System.Single ViewBorders::borderTop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOutFromBottom(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOutFromLeft(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOutFromRight(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOnBorderTop(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOnBorderBottom(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOnBorderLeft(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Single ViewBorders::borderLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderLeft_m7C50A44636CE5AEEC9BE685BF200E100A5CA02C1 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOnBorderRight(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_position(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_position_m1604084713EB195D44B8B411D4BCAFA5941E3413 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Void ManageLifeBar::RemoveToLife(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageLifeBar_RemoveToLife_mD92710734AE395928F87BFE55661FB8AA6A1368D (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, int32_t ___toRemove0, const RuntimeMethod* method);
// System.Int32 ManageLifeBar::GetLife()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8_inline (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, const RuntimeMethod* method);
// System.Void ManageAttributes::RestartLife()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageAttributes_RestartLife_mE1F2FB13FDD3F458547FF98DB025C247C114CD43 (ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_m9829583AE3BF1285861C580895202F760F3A82E8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, String_t* ___name0, TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___components1, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<MoveShip>()
inline MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * GameObject_GetComponent_TisMoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A_m9535F8179E6E9064B77556325A1AED291369EB53 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Vector2 MoveShip::getInput()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749_inline (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, const RuntimeMethod* method);
// System.Boolean ViewBorders::isOutFromTop(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromTop_mE6047837B4FFA21B6B70486AF6821944D9A44EAA (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DestructionEffectEnemy::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestructionEffectEnemy_Start_m2A239827B2CEC541399FB49A8FFB86846AABDFA0 (DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// color = new Color(1, 0, 0, 1);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_0), (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_color_4(L_0);
		// gameObject.GetComponent<SpriteRenderer>().color = color;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2;
		L_2 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_1, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = __this->get_color_4();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DestructionEffectEnemy::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestructionEffectEnemy_Update_m8ABC902696206AEBE0FC84448F6180A408BA6D54 (DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (color.r < 1) { color.g += 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_0 = __this->get_address_of_color_4();
		float L_1 = L_0->get_r_0();
		if ((!(((float)L_1) < ((float)(1.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		// if (color.r < 1) { color.g += 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_2 = __this->get_address_of_color_4();
		float* L_3 = L_2->get_address_of_g_1();
		float* L_4 = L_3;
		float L_5 = *((float*)L_4);
		*((float*)L_4) = (float)((float)il2cpp_codegen_add((float)L_5, (float)(0.0299999993f)));
	}

IL_0026:
	{
		// if (color.g < 1) { color.g += 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_6 = __this->get_address_of_color_4();
		float L_7 = L_6->get_g_1();
		if ((!(((float)L_7) < ((float)(1.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		// if (color.g < 1) { color.g += 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_8 = __this->get_address_of_color_4();
		float* L_9 = L_8->get_address_of_g_1();
		float* L_10 = L_9;
		float L_11 = *((float*)L_10);
		*((float*)L_10) = (float)((float)il2cpp_codegen_add((float)L_11, (float)(0.0299999993f)));
	}

IL_004c:
	{
		// if (color.b < 1) { color.b += 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_12 = __this->get_address_of_color_4();
		float L_13 = L_12->get_b_2();
		if ((!(((float)L_13) < ((float)(1.0f)))))
		{
			goto IL_0072;
		}
	}
	{
		// if (color.b < 1) { color.b += 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_14 = __this->get_address_of_color_4();
		float* L_15 = L_14->get_address_of_b_2();
		float* L_16 = L_15;
		float L_17 = *((float*)L_16);
		*((float*)L_16) = (float)((float)il2cpp_codegen_add((float)L_17, (float)(0.0299999993f)));
	}

IL_0072:
	{
		// if (color.a > 0) { color.a -= 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_18 = __this->get_address_of_color_4();
		float L_19 = L_18->get_a_3();
		if ((!(((float)L_19) > ((float)(0.0f)))))
		{
			goto IL_0098;
		}
	}
	{
		// if (color.a > 0) { color.a -= 0.03f; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_20 = __this->get_address_of_color_4();
		float* L_21 = L_20->get_address_of_a_3();
		float* L_22 = L_21;
		float L_23 = *((float*)L_22);
		*((float*)L_22) = (float)((float)il2cpp_codegen_subtract((float)L_23, (float)(0.0299999993f)));
	}

IL_0098:
	{
		// gameObject.GetComponent<SpriteRenderer>().color = color;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24;
		L_24 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_25;
		L_25 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_24, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_26 = __this->get_color_4();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_25, L_26, /*hidden argument*/NULL);
		// if (color.a <= 0) { Destroy(gameObject); }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_27 = __this->get_address_of_color_4();
		float L_28 = L_27->get_a_3();
		if ((!(((float)L_28) <= ((float)(0.0f)))))
		{
			goto IL_00cb;
		}
	}
	{
		// if (color.a <= 0) { Destroy(gameObject); }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29;
		L_29 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_29, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		// }
		return;
	}
}
// System.Void DestructionEffectEnemy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestructionEffectEnemy__ctor_mE3392720B83AE27F0062F301A20B73C55DA1DD02 (DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DestructionEffectLaser::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestructionEffectLaser_Start_mEE40BDCFAA8606867D4DE10BE748DBC686967E23 (DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// childrensIndex = new List<int>();
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)il2cpp_codegen_object_new(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_il2cpp_TypeInfo_var);
		List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD(L_0, /*hidden argument*/List_1__ctor_m45E78772E9157F6CD684A69AAB07CE4082FE5FFD_RuntimeMethod_var);
		__this->set_childrensIndex_4(L_0);
		// for (int i=0; i<gameObject.GetComponent<Transform>().childCount; i++) {
		V_0 = 0;
		goto IL_003c;
	}

IL_000f:
	{
		// GameObject child = gameObject.GetComponent<Transform>().GetChild(i).gameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_1, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		int32_t L_3 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_2, L_3, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_4, /*hidden argument*/NULL);
		// if (child.activeSelf) { childrensIndex.Add(i); }
		bool L_6;
		L_6 = GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		// if (child.activeSelf) { childrensIndex.Add(i); }
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_7 = __this->get_childrensIndex_4();
		int32_t L_8 = V_0;
		List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F(L_7, L_8, /*hidden argument*/List_1_Add_mEE653047BDB3486ACC2E16DC6C3422A0BA48F01F_RuntimeMethod_var);
	}

IL_0038:
	{
		// for (int i=0; i<gameObject.GetComponent<Transform>().childCount; i++) {
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_003c:
	{
		// for (int i=0; i<gameObject.GetComponent<Transform>().childCount; i++) {
		int32_t L_10 = V_0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_11, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		int32_t L_13;
		L_13 = Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_13)))
		{
			goto IL_000f;
		}
	}
	{
		// if (childrensIndex.Count > 0) {
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_14 = __this->get_childrensIndex_4();
		int32_t L_15;
		L_15 = List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_inline(L_14, /*hidden argument*/List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_RuntimeMethod_var);
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_008f;
		}
	}
	{
		// color = gameObject.GetComponent<Transform>().GetChild(childrensIndex[0]).gameObject.GetComponent<SpriteRenderer>().color;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16;
		L_16 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_16, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_18 = __this->get_childrensIndex_4();
		int32_t L_19;
		L_19 = List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_inline(L_18, 0, /*hidden argument*/List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_17, L_19, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_21;
		L_21 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_20, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_22;
		L_22 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_21, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_23;
		L_23 = SpriteRenderer_get_color_mAE96B8C6754CBE7820863BD5E97729B5DBF96EAC(L_22, /*hidden argument*/NULL);
		__this->set_color_5(L_23);
		// }
		return;
	}

IL_008f:
	{
		// color = new Color(1, 1, 1, 1);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_24;
		memset((&L_24), 0, sizeof(L_24));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_24), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_color_5(L_24);
		// }
		return;
	}
}
// System.Void DestructionEffectLaser::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestructionEffectLaser_Update_m8E52C4FE4399EEB7E6151DFCAB4B134BD5C268D5 (DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m0F4FCA57A586D78D592E624FE089FC61DF99EF86_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m153808182EE4702E05177827BB9D2D3961116B24_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (color.a > 0) {
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_0 = __this->get_address_of_color_5();
		float L_1 = L_0->get_a_3();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_007b;
		}
	}
	{
		// color.a -= 0.03f;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_2 = __this->get_address_of_color_5();
		float* L_3 = L_2->get_address_of_a_3();
		float* L_4 = L_3;
		float L_5 = *((float*)L_4);
		*((float*)L_4) = (float)((float)il2cpp_codegen_subtract((float)L_5, (float)(0.0299999993f)));
		// foreach (int i in childrensIndex) {
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_6 = __this->get_childrensIndex_4();
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_7;
		L_7 = List_1_GetEnumerator_m153808182EE4702E05177827BB9D2D3961116B24(L_6, /*hidden argument*/List_1_GetEnumerator_m153808182EE4702E05177827BB9D2D3961116B24_RuntimeMethod_var);
		V_0 = L_7;
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0062;
		}

IL_0034:
		{
			// foreach (int i in childrensIndex) {
			int32_t L_8;
			L_8 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(&V_0), /*hidden argument*/Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_RuntimeMethod_var);
			V_1 = L_8;
			// gameObject.GetComponent<Transform>().GetChild(i).gameObject.GetComponent<SpriteRenderer>().color = color;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
			L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
			L_10 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_9, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
			int32_t L_11 = V_1;
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
			L_12 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_10, L_11, /*hidden argument*/NULL);
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
			L_13 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_12, /*hidden argument*/NULL);
			SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_14;
			L_14 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_13, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
			Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_15 = __this->get_color_5();
			SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_14, L_15, /*hidden argument*/NULL);
		}

IL_0062:
		{
			// foreach (int i in childrensIndex) {
			bool L_16;
			L_16 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_RuntimeMethod_var);
			if (L_16)
			{
				goto IL_0034;
			}
		}

IL_006b:
		{
			IL2CPP_LEAVE(0x86, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m0F4FCA57A586D78D592E624FE089FC61DF99EF86((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(&V_0), /*hidden argument*/Enumerator_Dispose_m0F4FCA57A586D78D592E624FE089FC61DF99EF86_RuntimeMethod_var);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x86, IL_0086)
	}

IL_007b:
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_17, /*hidden argument*/NULL);
	}

IL_0086:
	{
		// }
		return;
	}
}
// System.Void DestructionEffectLaser::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestructionEffectLaser__ctor_mF75384A5D028D953963E438F45B0C54ED26B0D1F (DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ManageAttributes::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageAttributes_Start_m42979D217C33A43A157787CA782057713CC25D82 (ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * __this, const RuntimeMethod* method)
{
	{
		// restOfLifePoints = lifePoints;
		int32_t L_0 = __this->get_lifePoints_6();
		__this->set_restOfLifePoints_8(L_0);
		// }
		return;
	}
}
// System.Int32 ManageAttributes::AttackedBy(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9 (ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// restOfLifePoints -= other.GetComponent<ManageAttributes>().attackPoints;
		int32_t L_0 = __this->get_restOfLifePoints_8();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = ___other0;
		ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * L_2;
		L_2 = GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8(L_1, /*hidden argument*/GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		int32_t L_3 = L_2->get_attackPoints_5();
		__this->set_restOfLifePoints_8(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_3)));
		// return restOfLifePoints;
		int32_t L_4 = __this->get_restOfLifePoints_8();
		return L_4;
	}
}
// System.Int32 ManageAttributes::getLifePoints()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ManageAttributes_getLifePoints_mA744A7D2223A6FD87137A28794FE7B20F4EC79E3 (ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * __this, const RuntimeMethod* method)
{
	{
		// return restOfLifePoints;
		int32_t L_0 = __this->get_restOfLifePoints_8();
		return L_0;
	}
}
// System.Void ManageAttributes::RestartLife()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageAttributes_RestartLife_mE1F2FB13FDD3F458547FF98DB025C247C114CD43 (ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * __this, const RuntimeMethod* method)
{
	{
		// restOfLifePoints = lifePoints;
		int32_t L_0 = __this->get_lifePoints_6();
		__this->set_restOfLifePoints_8(L_0);
		// }
		return;
	}
}
// System.Void ManageAttributes::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageAttributes__ctor_mB772E6BCEBF8BFF7D0941864A188BFBCD3889569 (ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ManageEnemy::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageEnemy_Start_mE121432A329522875EE8D1E5DF84410B6F64FFEC (ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// generator = new System.Random();
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_0 = (Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 *)il2cpp_codegen_object_new(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var);
		Random__ctor_mF40AD1812BABC06235B661CCE513E4F74EEE9F05(L_0, /*hidden argument*/NULL);
		__this->set_generator_7(L_0);
		// viewBorderLeftTop = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_1;
		L_1 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_2), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Camera_ViewportToWorldPoint_m1273EE3868551C6FF551ABA9A76DC7D66E883116(L_1, L_2, /*hidden argument*/NULL);
		__this->set_viewBorderLeftTop_8(L_3);
		// viewBorderRightTop = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_4;
		L_4 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_5), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Camera_ViewportToWorldPoint_m1273EE3868551C6FF551ABA9A76DC7D66E883116(L_4, L_5, /*hidden argument*/NULL);
		__this->set_viewBorderRightTop_9(L_6);
		// }
		return;
	}
}
// System.Void ManageEnemy::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageEnemy_Update_m2C1F82864863B910C60D749433D7D2208920DE3F (ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* V_0 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// GameObject[] respawns = GameObject.FindGameObjectsWithTag(prefabEnnemy.tag);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_prefabEnnemy_6();
		String_t* L_1;
		L_1 = GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33(L_0, /*hidden argument*/NULL);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2;
		L_2 = GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (respawns.Length < maxNumberOfMeteor) {
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_3 = V_0;
		int32_t L_4 = __this->get_maxNumberOfMeteor_5();
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length)))) >= ((int32_t)L_4)))
		{
			goto IL_0074;
		}
	}
	{
		// if (respawns.Length < minNumberOfMeteor || ( respawns.Length >= minNumberOfMeteor && generator.Next(1, 10) <= 2)) {
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_5 = V_0;
		int32_t L_6 = __this->get_minNumberOfMeteor_4();
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length)))) < ((int32_t)L_6)))
		{
			goto IL_0043;
		}
	}
	{
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_7 = V_0;
		int32_t L_8 = __this->get_minNumberOfMeteor_4();
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))) < ((int32_t)L_8)))
		{
			goto IL_0074;
		}
	}
	{
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_9 = __this->get_generator_7();
		int32_t L_10;
		L_10 = VirtFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(5 /* System.Int32 System.Random::Next(System.Int32,System.Int32) */, L_9, 1, ((int32_t)10));
		if ((((int32_t)L_10) > ((int32_t)2)))
		{
			goto IL_0074;
		}
	}

IL_0043:
	{
		// Vector3 size = prefabEnnemy.GetComponent<SpriteRenderer>().bounds.size;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11 = __this->get_prefabEnnemy_6();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_12;
		L_12 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_11, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_13;
		L_13 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_2), /*hidden argument*/NULL);
		V_1 = L_14;
		// Instantiate(prefabEnnemy, RandomPosition(size), Quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_prefabEnnemy_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = ManageEnemy_RandomPosition_mAB1D66F48604EEBFD52F246843DD5DAE7E62086D(__this, L_16, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_18;
		L_18 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19;
		L_19 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_15, L_17, L_18, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
	}

IL_0074:
	{
		// }
		return;
	}
}
// UnityEngine.Vector3 ManageEnemy::RandomPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ManageEnemy_RandomPosition_mAB1D66F48604EEBFD52F246843DD5DAE7E62086D (ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___size0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 position = new Vector3(0, 0, 0);
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// position.x = Random.Range(viewBorderLeftTop.x + size.x/2, viewBorderRightTop.x - size.x/2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_viewBorderLeftTop_8();
		float L_1 = L_0->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___size0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_4 = __this->get_address_of_viewBorderRightTop_9();
		float L_5 = L_4->get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___size0;
		float L_7 = L_6.get_x_2();
		float L_8;
		L_8 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((float)il2cpp_codegen_add((float)L_1, (float)((float)((float)L_3/(float)(2.0f))))), ((float)il2cpp_codegen_subtract((float)L_5, (float)((float)((float)L_7/(float)(2.0f))))), /*hidden argument*/NULL);
		(&V_0)->set_x_2(L_8);
		// position.y = viewBorderLeftTop.y + size.y;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_9 = __this->get_address_of_viewBorderLeftTop_8();
		float L_10 = L_9->get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___size0;
		float L_12 = L_11.get_y_3();
		(&V_0)->set_y_3(((float)il2cpp_codegen_add((float)L_10, (float)L_12)));
		// return position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
// System.Void ManageEnemy::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageEnemy__ctor_m83E39BE85DA2DC1ACC713D9CBE2351CA24F14E2B (ManageEnemy_tD3F2F36550DB98A3358A45C6863399B88163C7EC * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ManageGame::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_Start_m9216CD6AE375098C1CB437ABA04CA63CEA5EAAEE (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isPaused = false;
		__this->set_isPaused_6((bool)0);
		// if (Instance == null) {
		ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * L_0 = ((ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_StaticFields*)il2cpp_codegen_static_fields_for(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		// Instance = this;
		((ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_StaticFields*)il2cpp_codegen_static_fields_for(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var))->set_Instance_4(__this);
		// DontDestroyOnLoad(Instance.gameObject);
		ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * L_2 = ((ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_StaticFields*)il2cpp_codegen_static_fields_for(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var))->get_Instance_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_3, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002a:
	{
		// else if (this != Instance) {
		ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * L_4 = ((ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_StaticFields*)il2cpp_codegen_static_fields_for(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(__this, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_6, /*hidden argument*/NULL);
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void ManageGame::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_Update_mA0A47EFDEDB14FEE6256A7CE372F0867E034DCEA (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method)
{
	{
		// if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Pause) || Input.GetKeyDown(KeyCode.P)) {
		bool L_0;
		L_0 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)27), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		bool L_1;
		L_1 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)19), /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		bool L_2;
		L_2 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)112), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}

IL_001b:
	{
		// if (isPaused) { ContinueGameLevel(); }
		bool L_3 = __this->get_isPaused_6();
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		// if (isPaused) { ContinueGameLevel(); }
		ManageGame_ContinueGameLevel_m9371DB8D650C9A6372522C0E1F99FF24413E3A99(__this, /*hidden argument*/NULL);
		// if (isPaused) { ContinueGameLevel(); }
		return;
	}

IL_002a:
	{
		// else { PauseGameLevel(); }
		ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4(__this, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void ManageGame::ShowScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_ShowScene_m2D10543862AA8077F31C20FBAE91933A7920E318 (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, String_t* ___sceneName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log(sceneName);
		String_t* L_0 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_0, /*hidden argument*/NULL);
		// SceneManager.LoadScene(sceneName);
		String_t* L_1 = ___sceneName0;
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ManageGame::QuitGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_QuitGame_mD886C1B313AC14E2D780E85A772C3438CF55D94E (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method)
{
	{
		// Application.Quit();
		Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A(/*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ManageGame::PauseGameLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4 (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2AD47C03F7A83F82E3B2ADFE8A60F1727FD3BEFD);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gamePauseMenu.GetComponent<Animator>().SetTrigger("start");
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_gamePauseMenu_5();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1;
		L_1 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_1, _stringLiteral2AD47C03F7A83F82E3B2ADFE8A60F1727FD3BEFD, /*hidden argument*/NULL);
		// Time.timeScale = 0;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((0.0f), /*hidden argument*/NULL);
		// isPaused = true;
		__this->set_isPaused_6((bool)1);
		// }
		return;
	}
}
// System.Void ManageGame::ContinueGameLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame_ContinueGameLevel_m9371DB8D650C9A6372522C0E1F99FF24413E3A99 (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gamePauseMenu.GetComponent<Animator>().SetTrigger("stop");
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_gamePauseMenu_5();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1;
		L_1 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_0, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_1, _stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7, /*hidden argument*/NULL);
		// Time.timeScale = 1;
		Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA((1.0f), /*hidden argument*/NULL);
		// isPaused = false;
		__this->set_isPaused_6((bool)0);
		// }
		return;
	}
}
// System.Void ManageGame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageGame__ctor_mA6B7A7CE4D286460F77AB226E334C14B0CC2E780 (ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ManageLifeBar::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageLifeBar_Start_m44A0AA415C799AC8D66D3BE10A703D781C93688F (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// currentLife = initLife;
		int32_t L_0 = __this->get_initLife_5();
		__this->set_currentLife_10(L_0);
		// if (Instance == null) {
		ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * L_1 = ((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		// Instance = this;
		((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->set_Instance_4(__this);
		// DontDestroyOnLoad(Instance.gameObject);
		ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * L_3 = ((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->get_Instance_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_4, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002f:
	{
		// else if (this != Instance) {
		ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * L_5 = ((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(__this, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0047;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_7, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// }
		return;
	}
}
// System.Void ManageLifeBar::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageLifeBar_Update_mD0FBC92BCE43B59C8A5D87643662D894D00D2CEF (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	String_t* V_1 = NULL;
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppChar V_5 = 0x0;
	String_t* G_B5_0 = NULL;
	String_t* G_B4_0 = NULL;
	{
		// foreach (GameObject number in GameObject.FindGameObjectsWithTag(newTag)) {
		String_t* L_0 = __this->get_newTag_6();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_1;
		L_1 = GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		V_3 = 0;
		goto IL_001c;
	}

IL_0010:
	{
		// foreach (GameObject number in GameObject.FindGameObjectsWithTag(newTag)) {
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = V_2;
		int32_t L_3 = V_3;
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		// Destroy(number);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_001c:
	{
		// foreach (GameObject number in GameObject.FindGameObjectsWithTag(newTag)) {
		int32_t L_7 = V_3;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_8 = V_2;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length))))))
		{
			goto IL_0010;
		}
	}
	{
		// Vector3 position = gameObject.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_9, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		// position.x += offsetX;
		float* L_12 = (&V_0)->get_address_of_x_2();
		float* L_13 = L_12;
		float L_14 = *((float*)L_13);
		float L_15 = __this->get_offsetX_8();
		*((float*)L_13) = (float)((float)il2cpp_codegen_add((float)L_14, (float)L_15));
		// string currentLifeStr = "" + currentLife;
		int32_t* L_16 = __this->get_address_of_currentLife_10();
		String_t* L_17;
		L_17 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_16, /*hidden argument*/NULL);
		String_t* L_18 = L_17;
		G_B4_0 = L_18;
		if (L_18)
		{
			G_B5_0 = L_18;
			goto IL_0058;
		}
	}
	{
		G_B5_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0058:
	{
		V_1 = G_B5_0;
		// for (int i=0; i<currentLifeStr.Length; i++) {
		V_4 = 0;
		goto IL_00aa;
	}

IL_005e:
	{
		// position.x += space;
		float* L_19 = (&V_0)->get_address_of_x_2();
		float* L_20 = L_19;
		float L_21 = *((float*)L_20);
		float L_22 = __this->get_space_9();
		*((float*)L_20) = (float)((float)il2cpp_codegen_add((float)L_21, (float)L_22));
		// GameObject prefab = prefabNumbers[int.Parse(currentLifeStr[i].ToString())];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_23 = __this->get_prefabNumbers_7();
		String_t* L_24 = V_1;
		int32_t L_25 = V_4;
		Il2CppChar L_26;
		L_26 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_24, L_25, /*hidden argument*/NULL);
		V_5 = L_26;
		String_t* L_27;
		L_27 = Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8((Il2CppChar*)(&V_5), /*hidden argument*/NULL);
		int32_t L_28;
		L_28 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_27, /*hidden argument*/NULL);
		int32_t L_29 = L_28;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_30 = (L_23)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_29));
		// prefab.tag = newTag;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31 = L_30;
		String_t* L_32 = __this->get_newTag_6();
		GameObject_set_tag_m0EBA46574304C71E047A33BDD5F5D49E9D9A25BE(L_31, L_32, /*hidden argument*/NULL);
		// Instantiate(prefab, position, Quaternion.identity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_34;
		L_34 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_35;
		L_35 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_31, L_33, L_34, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// for (int i=0; i<currentLifeStr.Length; i++) {
		int32_t L_36 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)1));
	}

IL_00aa:
	{
		// for (int i=0; i<currentLifeStr.Length; i++) {
		int32_t L_37 = V_4;
		String_t* L_38 = V_1;
		int32_t L_39;
		L_39 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_38, /*hidden argument*/NULL);
		if ((((int32_t)L_37) < ((int32_t)L_39)))
		{
			goto IL_005e;
		}
	}
	{
		// }
		return;
	}
}
// System.Void ManageLifeBar::AddToLife(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageLifeBar_AddToLife_m89DF759F658CB7AB76CD8E059C34E93D04E166E3 (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, int32_t ___toAdd0, const RuntimeMethod* method)
{
	{
		// currentLife += toAdd;
		int32_t L_0 = __this->get_currentLife_10();
		int32_t L_1 = ___toAdd0;
		__this->set_currentLife_10(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		// }
		return;
	}
}
// System.Void ManageLifeBar::RemoveToLife(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageLifeBar_RemoveToLife_mD92710734AE395928F87BFE55661FB8AA6A1368D (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, int32_t ___toRemove0, const RuntimeMethod* method)
{
	{
		// currentLife -= toRemove;
		int32_t L_0 = __this->get_currentLife_10();
		int32_t L_1 = ___toRemove0;
		__this->set_currentLife_10(((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1)));
		// if (currentLife < 0) { currentLife = 0; }
		int32_t L_2 = __this->get_currentLife_10();
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001e;
		}
	}
	{
		// if (currentLife < 0) { currentLife = 0; }
		__this->set_currentLife_10(0);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Int32 ManageLifeBar::GetLife()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8 (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, const RuntimeMethod* method)
{
	{
		// return currentLife;
		int32_t L_0 = __this->get_currentLife_10();
		return L_0;
	}
}
// System.Void ManageLifeBar::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManageLifeBar__ctor_m4F28E430C644E499708D8ABBF307B37230DB4B7B (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ManagerScore::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerScore_Start_mDDFAC952DBBDFB4915CA9C9A97D633F2308A91E5 (ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Instance == null) {
		ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * L_0 = ((ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_StaticFields*)il2cpp_codegen_static_fields_for(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		// Instance = this;
		((ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_StaticFields*)il2cpp_codegen_static_fields_for(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var))->set_Instance_4(__this);
		// DontDestroyOnLoad(Instance.gameObject);
		ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * L_2 = ((ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_StaticFields*)il2cpp_codegen_static_fields_for(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var))->get_Instance_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9(L_3, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0023:
	{
		// else if (this != Instance) {
		ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * L_4 = ((ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_StaticFields*)il2cpp_codegen_static_fields_for(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var))->get_Instance_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(__this, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_6, /*hidden argument*/NULL);
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void ManagerScore::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerScore_Update_mB614E9A52C210E11FF01CCE6CBCED218E7C0C9C9 (ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	String_t* V_1 = NULL;
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Il2CppChar V_5 = 0x0;
	String_t* G_B5_0 = NULL;
	String_t* G_B4_0 = NULL;
	{
		// foreach (GameObject number in GameObject.FindGameObjectsWithTag(newTag)) {
		String_t* L_0 = __this->get_newTag_6();
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_1;
		L_1 = GameObject_FindGameObjectsWithTag_m0948320611DC82590D59A36D1C57155B1B6CE186(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		V_3 = 0;
		goto IL_001c;
	}

IL_0010:
	{
		// foreach (GameObject number in GameObject.FindGameObjectsWithTag(newTag)) {
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_2 = V_2;
		int32_t L_3 = V_3;
		int32_t L_4 = L_3;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		// Destroy(number);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_001c:
	{
		// foreach (GameObject number in GameObject.FindGameObjectsWithTag(newTag)) {
		int32_t L_7 = V_3;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_8 = V_2;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length))))))
		{
			goto IL_0010;
		}
	}
	{
		// Vector3 position = gameObject.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_9, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		// string scoreStr = "" + score;
		int32_t* L_12 = __this->get_address_of_score_9();
		String_t* L_13;
		L_13 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_12, /*hidden argument*/NULL);
		String_t* L_14 = L_13;
		G_B4_0 = L_14;
		if (L_14)
		{
			G_B5_0 = L_14;
			goto IL_0047;
		}
	}
	{
		G_B5_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0047:
	{
		V_1 = G_B5_0;
		// if (score == 0) { scoreStr += scoreStr + scoreStr; }
		int32_t L_15 = __this->get_score_9();
		if (L_15)
		{
			goto IL_0059;
		}
	}
	{
		// if (score == 0) { scoreStr += scoreStr + scoreStr; }
		String_t* L_16 = V_1;
		String_t* L_17 = V_1;
		String_t* L_18 = V_1;
		String_t* L_19;
		L_19 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_16, L_17, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
	}

IL_0059:
	{
		// for (int i=scoreStr.Length-1; i>-1; i--) {
		String_t* L_20 = V_1;
		int32_t L_21;
		L_21 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_20, /*hidden argument*/NULL);
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)1));
		goto IL_00b1;
	}

IL_0065:
	{
		// GameObject prefab = prefabNumbers[int.Parse(scoreStr[i].ToString())];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_22 = __this->get_prefabNumbers_7();
		String_t* L_23 = V_1;
		int32_t L_24 = V_4;
		Il2CppChar L_25;
		L_25 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_23, L_24, /*hidden argument*/NULL);
		V_5 = L_25;
		String_t* L_26;
		L_26 = Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8((Il2CppChar*)(&V_5), /*hidden argument*/NULL);
		int32_t L_27;
		L_27 = Int32_Parse_mE5D220FEA7F0BFB1B220B2A30797D7DD83ACF22C(L_26, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29 = (L_22)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_28));
		// prefab.tag = newTag;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_30 = L_29;
		String_t* L_31 = __this->get_newTag_6();
		GameObject_set_tag_m0EBA46574304C71E047A33BDD5F5D49E9D9A25BE(L_30, L_31, /*hidden argument*/NULL);
		// Instantiate(prefab, position, Quaternion.identity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_33;
		L_33 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_34;
		L_34 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_30, L_32, L_33, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// position.x -= space;
		float* L_35 = (&V_0)->get_address_of_x_2();
		float* L_36 = L_35;
		float L_37 = *((float*)L_36);
		float L_38 = __this->get_space_8();
		*((float*)L_36) = (float)((float)il2cpp_codegen_subtract((float)L_37, (float)L_38));
		// for (int i=scoreStr.Length-1; i>-1; i--) {
		int32_t L_39 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_39, (int32_t)1));
	}

IL_00b1:
	{
		// for (int i=scoreStr.Length-1; i>-1; i--) {
		int32_t L_40 = V_4;
		if ((((int32_t)L_40) > ((int32_t)(-1))))
		{
			goto IL_0065;
		}
	}
	{
		// }
		return;
	}
}
// System.Void ManagerScore::AddToScore(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerScore_AddToScore_m0AC27C2FCF200C3CCCAA432A6B572495FE9F5775 (ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * __this, int32_t ___toAdd0, const RuntimeMethod* method)
{
	{
		// score += toAdd;
		int32_t L_0 = __this->get_score_9();
		int32_t L_1 = ___toAdd0;
		__this->set_score_9(((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1)));
		// }
		return;
	}
}
// System.Void ManagerScore::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ManagerScore__ctor_m2817EA953C04C9D3D29D0423A40954A5A410EC21 (ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoveBackground::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveBackground_Start_m73952C44ECDB8E02A7C84CE140363C3158096CD3 (MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// viewBorders = Camera.main.GetComponent<ViewBorders>();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_1;
		L_1 = Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548(L_0, /*hidden argument*/Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		__this->set_viewBorders_10(L_1);
		// sizeY = background1.GetComponent<SpriteRenderer>().bounds.size.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_background1_4();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3;
		L_3 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_2, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_4;
		L_4 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), /*hidden argument*/NULL);
		float L_6 = L_5.get_y_3();
		__this->set_sizeY_11(L_6);
		// toRight = true;
		__this->set_toRight_9((bool)1);
		// }
		return;
	}
}
// System.Void MoveBackground::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveBackground_Update_m233EF7D84EBF4ED003A6EC2F9942DD701BABDC2E (MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (toRight) {
		bool L_0 = __this->get_toRight_9();
		if (!L_0)
		{
			goto IL_00de;
		}
	}
	{
		// if (background1.GetComponent<Transform>().position.x < offsetX) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_1, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		float L_4 = L_3.get_x_2();
		float L_5 = __this->get_offsetX_7();
		if ((!(((float)L_4) < ((float)L_5))))
		{
			goto IL_00d2;
		}
	}
	{
		// background1.GetComponent<Rigidbody2D>().velocity = new Vector3(speed.x, -speed.y, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_background1_4();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_7;
		L_7 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_6, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_8 = __this->get_address_of_speed_8();
		float L_9 = L_8->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_10 = __this->get_address_of_speed_8();
		float L_11 = L_10->get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), L_9, ((-L_11)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		L_13 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_12, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_7, L_13, /*hidden argument*/NULL);
		// background2.GetComponent<Rigidbody2D>().velocity = new Vector3(speed.x, -speed.y, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = __this->get_background2_5();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_15;
		L_15 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_14, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_16 = __this->get_address_of_speed_8();
		float L_17 = L_16->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_18 = __this->get_address_of_speed_8();
		float L_19 = L_18->get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_20), L_17, ((-L_19)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21;
		L_21 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_20, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_15, L_21, /*hidden argument*/NULL);
		// background3.GetComponent<Rigidbody2D>().velocity = new Vector3(speed.x, -speed.y, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = __this->get_background3_6();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_23;
		L_23 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_22, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_24 = __this->get_address_of_speed_8();
		float L_25 = L_24->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_26 = __this->get_address_of_speed_8();
		float L_27 = L_26->get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_28), L_25, ((-L_27)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_29;
		L_29 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_28, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_23, L_29, /*hidden argument*/NULL);
		// }
		goto IL_01ad;
	}

IL_00d2:
	{
		// toRight = false;
		__this->set_toRight_9((bool)0);
		// }
		goto IL_01ad;
	}

IL_00de:
	{
		// if (background1.GetComponent<Transform>().position.x > -offsetX) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_30 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31;
		L_31 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_30, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_31, /*hidden argument*/NULL);
		float L_33 = L_32.get_x_2();
		float L_34 = __this->get_offsetX_7();
		if ((!(((float)L_33) > ((float)((-L_34))))))
		{
			goto IL_01a6;
		}
	}
	{
		// background1.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed.x, -speed.y, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_35 = __this->get_background1_4();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_36;
		L_36 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_35, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_37 = __this->get_address_of_speed_8();
		float L_38 = L_37->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_39 = __this->get_address_of_speed_8();
		float L_40 = L_39->get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_41;
		memset((&L_41), 0, sizeof(L_41));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_41), ((-L_38)), ((-L_40)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_42;
		L_42 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_41, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_36, L_42, /*hidden argument*/NULL);
		// background2.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed.x, -speed.y, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_43 = __this->get_background2_5();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_44;
		L_44 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_43, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_45 = __this->get_address_of_speed_8();
		float L_46 = L_45->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_47 = __this->get_address_of_speed_8();
		float L_48 = L_47->get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49;
		memset((&L_49), 0, sizeof(L_49));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_49), ((-L_46)), ((-L_48)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_50;
		L_50 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_49, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_44, L_50, /*hidden argument*/NULL);
		// background3.GetComponent<Rigidbody2D>().velocity = new Vector3(-speed.x, -speed.y, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_51 = __this->get_background3_6();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_52;
		L_52 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_51, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_53 = __this->get_address_of_speed_8();
		float L_54 = L_53->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_55 = __this->get_address_of_speed_8();
		float L_56 = L_55->get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_57;
		memset((&L_57), 0, sizeof(L_57));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_57), ((-L_54)), ((-L_56)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_58;
		L_58 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_57, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_52, L_58, /*hidden argument*/NULL);
		// }
		goto IL_01ad;
	}

IL_01a6:
	{
		// toRight = true;
		__this->set_toRight_9((bool)1);
	}

IL_01ad:
	{
		// if (background1.GetComponent<Transform>().position.y + (sizeY/2) < viewBorders.borderBottom()) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_59 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_60;
		L_60 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_59, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_61;
		L_61 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_60, /*hidden argument*/NULL);
		float L_62 = L_61.get_y_3();
		float L_63 = __this->get_sizeY_11();
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_64 = __this->get_viewBorders_10();
		float L_65;
		L_65 = ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6(L_64, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_62, (float)((float)((float)L_63/(float)(2.0f)))))) < ((float)L_65))))
		{
			goto IL_0227;
		}
	}
	{
		// Vector3 position = background1.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_66 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_67;
		L_67 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_66, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_68;
		L_68 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_67, /*hidden argument*/NULL);
		V_0 = L_68;
		// position.y = background3.GetComponent<Transform>().position.y + (sizeY/2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_69 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_70;
		L_70 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_69, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_71;
		L_71 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_70, /*hidden argument*/NULL);
		float L_72 = L_71.get_y_3();
		float L_73 = __this->get_sizeY_11();
		(&V_0)->set_y_3(((float)il2cpp_codegen_add((float)L_72, (float)((float)((float)L_73/(float)(2.0f))))));
		// background1.GetComponent<Transform>().position = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_74 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_75;
		L_75 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_74, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_76 = V_0;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_75, L_76, /*hidden argument*/NULL);
	}

IL_0227:
	{
		// if (background2.GetComponent<Transform>().position.y + (sizeY/2) < viewBorders.borderBottom()) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_77 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_78;
		L_78 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_77, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_79;
		L_79 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_78, /*hidden argument*/NULL);
		float L_80 = L_79.get_y_3();
		float L_81 = __this->get_sizeY_11();
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_82 = __this->get_viewBorders_10();
		float L_83;
		L_83 = ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6(L_82, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_80, (float)((float)((float)L_81/(float)(2.0f)))))) < ((float)L_83))))
		{
			goto IL_02a1;
		}
	}
	{
		// Vector3 position = background2.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_84 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_85;
		L_85 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_84, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_86;
		L_86 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_85, /*hidden argument*/NULL);
		V_1 = L_86;
		// position.y = background1.GetComponent<Transform>().position.y + (sizeY/2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_87 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_88;
		L_88 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_87, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_89;
		L_89 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_88, /*hidden argument*/NULL);
		float L_90 = L_89.get_y_3();
		float L_91 = __this->get_sizeY_11();
		(&V_1)->set_y_3(((float)il2cpp_codegen_add((float)L_90, (float)((float)((float)L_91/(float)(2.0f))))));
		// background2.GetComponent<Transform>().position = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_92 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_93;
		L_93 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_92, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_94 = V_1;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_93, L_94, /*hidden argument*/NULL);
	}

IL_02a1:
	{
		// if (background3.GetComponent<Transform>().position.y + (sizeY/2) < viewBorders.borderBottom()) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_95 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_96;
		L_96 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_95, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_97;
		L_97 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_96, /*hidden argument*/NULL);
		float L_98 = L_97.get_y_3();
		float L_99 = __this->get_sizeY_11();
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_100 = __this->get_viewBorders_10();
		float L_101;
		L_101 = ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6(L_100, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_98, (float)((float)((float)L_99/(float)(2.0f)))))) < ((float)L_101))))
		{
			goto IL_031b;
		}
	}
	{
		// Vector3 position = background3.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_102 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_103;
		L_103 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_102, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_104;
		L_104 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_103, /*hidden argument*/NULL);
		V_2 = L_104;
		// position.y = background2.GetComponent<Transform>().position.y + (sizeY/2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_105 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_106;
		L_106 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_105, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_107;
		L_107 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_106, /*hidden argument*/NULL);
		float L_108 = L_107.get_y_3();
		float L_109 = __this->get_sizeY_11();
		(&V_2)->set_y_3(((float)il2cpp_codegen_add((float)L_108, (float)((float)((float)L_109/(float)(2.0f))))));
		// background3.GetComponent<Transform>().position = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_110 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_111;
		L_111 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_110, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_112 = V_2;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_111, L_112, /*hidden argument*/NULL);
	}

IL_031b:
	{
		// }
		return;
	}
}
// System.Void MoveBackground::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveBackground__ctor_mAF0741CCE7230F5038EA97EDD54679F1D20DF22D (MoveBackground_tBD2CDDE15D8FA66AB1851827EA4C22952A305988 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoveLaser1::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveLaser1_Start_m65994D75F536382A7BCEC75AD2CBBFE6A0395EFC (MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// viewBorders = Camera.main.GetComponent<ViewBorders>();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_1;
		L_1 = Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548(L_0, /*hidden argument*/Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		__this->set_viewBorders_5(L_1);
		// gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_3;
		L_3 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_2, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		float L_4 = __this->get_speed_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_5), (0.0f), L_4, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_3, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveLaser1::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveLaser1_Update_mB14CB11907F598D69B541F78D97EF9613CFF234B (MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (viewBorders.isOut(gameObject.GetComponent<Transform>().GetChild(0).gameObject)) {
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_0 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_1, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_2, 0, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = ViewBorders_isOut_m0A983F94132A2262E813A7FD17DDC6B666A5AC3C(L_0, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void MoveLaser1::OnCollisionEnter2D(UnityEngine.Collision2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveLaser1_OnCollisionEnter2D_mE7A09A80DDF61607DA9902FCAC475D17F8805371 (MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B * __this, Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisDestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C_mD62FF1BD23C0599E9CFDC98E4ADE20C7937244B1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisDestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1_mE2328AB002D256692BFE0A89BB2140F39E760423_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D_m734FCF5A42A89193927B270F69F5A1CBCA00FD82_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// GameObject other = collision.gameObject;
		Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * L_0 = ___collision0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Collision2D_get_gameObject_m6F07B9CA1FAD187933EB6D8E44BD9F870658F89F(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// gameObject.GetComponent<Transform>().GetChild(0).gameObject.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_2, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_3, 0, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_4, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)0, /*hidden argument*/NULL);
		// gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_7;
		L_7 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_6, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_8), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9;
		L_9 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_8, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_7, L_9, /*hidden argument*/NULL);
		// gameObject.GetComponent<Transform>().GetChild(1).gameObject.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10;
		L_10 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_10, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_12;
		L_12 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_11, 1, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_12, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_13, (bool)1, /*hidden argument*/NULL);
		// gameObject.GetComponent<Transform>().position = collision.GetContact(0).point;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_14, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * L_16 = ___collision0;
		ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62  L_17;
		L_17 = Collision2D_GetContact_m485AB3C5BE25E6CA83E7B7DA9524DBF9A94F4A77(L_16, 0, /*hidden argument*/NULL);
		V_1 = L_17;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_18;
		L_18 = ContactPoint2D_get_point_mB8EB8CD492D607A7E61553DEE618C70DB6787DEF((ContactPoint2D_t5A4C242ABAE740C565BF01A35CEE279058E66A62 *)(&V_1), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_18, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_15, L_19, /*hidden argument*/NULL);
		// gameObject.AddComponent<DestructionEffectLaser>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_20;
		L_20 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		DestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1 * L_21;
		L_21 = GameObject_AddComponent_TisDestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1_mE2328AB002D256692BFE0A89BB2140F39E760423(L_20, /*hidden argument*/GameObject_AddComponent_TisDestructionEffectLaser_t0DADAB878F89E7BC842B4139C5C98541417DB0A1_mE2328AB002D256692BFE0A89BB2140F39E760423_RuntimeMethod_var);
		// if (other.tag != gameObject.tag) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = V_0;
		String_t* L_23;
		L_23 = GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33(L_22, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24;
		L_24 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		String_t* L_25;
		L_25 = GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33(L_24, /*hidden argument*/NULL);
		bool L_26;
		L_26 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_23, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00eb;
		}
	}
	{
		// int otherLife = other.GetComponent<ManageAttributes>().AttackedBy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_27 = V_0;
		ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * L_28;
		L_28 = GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8(L_27, /*hidden argument*/GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29;
		L_29 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		int32_t L_30;
		L_30 = ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9(L_28, L_29, /*hidden argument*/NULL);
		// ManagerScore.Instance.AddToScore(other.GetComponent<ManageAttributes>().pointsToWin);
		ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695 * L_31 = ((ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_StaticFields*)il2cpp_codegen_static_fields_for(ManagerScore_t0811818CC9EBC7DAD567D47E2D0A43CDD280E695_il2cpp_TypeInfo_var))->get_Instance_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_32 = V_0;
		ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * L_33;
		L_33 = GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8(L_32, /*hidden argument*/GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		int32_t L_34 = L_33->get_pointsToWin_4();
		ManagerScore_AddToScore_m0AC27C2FCF200C3CCCAA432A6B572495FE9F5775(L_31, L_34, /*hidden argument*/NULL);
		// if (otherLife <= 0) {
		if ((((int32_t)L_30) > ((int32_t)0)))
		{
			goto IL_00eb;
		}
	}
	{
		// Destroy(other.GetComponent<PolygonCollider2D>());
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_35 = V_0;
		PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D * L_36;
		L_36 = GameObject_GetComponent_TisPolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D_m734FCF5A42A89193927B270F69F5A1CBCA00FD82(L_35, /*hidden argument*/GameObject_GetComponent_TisPolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D_m734FCF5A42A89193927B270F69F5A1CBCA00FD82_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_36, /*hidden argument*/NULL);
		// other.AddComponent<DestructionEffectEnemy>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_37 = V_0;
		DestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C * L_38;
		L_38 = GameObject_AddComponent_TisDestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C_mD62FF1BD23C0599E9CFDC98E4ADE20C7937244B1(L_37, /*hidden argument*/GameObject_AddComponent_TisDestructionEffectEnemy_tBF08DC51A80D12C11BE841C2BE61DF734D1F4A1C_mD62FF1BD23C0599E9CFDC98E4ADE20C7937244B1_RuntimeMethod_var);
	}

IL_00eb:
	{
		// }
		return;
	}
}
// System.Void MoveLaser1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveLaser1__ctor_m891E52378394329C7912E59536B4D5AED2E2C429 (MoveLaser1_tBE5D90837130FEC98AD8915FDE9E47DD0AE6188B * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoveMeteor::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveMeteor_Start_mAC824C78251B2E173E463ABE962D804D035D739B (MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// viewBorders = Camera.main.GetComponent<ViewBorders>();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_1;
		L_1 = Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548(L_0, /*hidden argument*/Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		__this->set_viewBorders_5(L_1);
		// size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3;
		L_3 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_2, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_4;
		L_4 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_3), /*hidden argument*/NULL);
		__this->set_size_6(L_5);
		// Vector3 velocity = new Vector3(0, 0, 0);
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// velocity.y = -Random.Range(0.5f, 1f) * speedMaxY;
		float L_6;
		L_6 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.5f), (1.0f), /*hidden argument*/NULL);
		float L_7 = __this->get_speedMaxY_4();
		(&V_0)->set_y_3(((float)il2cpp_codegen_multiply((float)((-L_6)), (float)L_7)));
		// float distanceToLeft = viewBorders.borderRight() + gameObject.GetComponent<Transform>().position.x;
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_8 = __this->get_viewBorders_5();
		float L_9;
		L_9 = ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B(L_8, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10;
		L_10 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_10, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_11, /*hidden argument*/NULL);
		float L_13 = L_12.get_x_2();
		V_1 = ((float)il2cpp_codegen_add((float)L_9, (float)L_13));
		// float distanceToRight = viewBorders.borderRight() * 2 - distanceToLeft;
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_14 = __this->get_viewBorders_5();
		float L_15;
		L_15 = ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B(L_14, /*hidden argument*/NULL);
		float L_16 = V_1;
		V_2 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_15, (float)(2.0f))), (float)L_16));
		// float distanceToBottom = viewBorders.borderTop() * 2 + gameObject.GetComponent<Transform>().position.y;
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_17 = __this->get_viewBorders_5();
		float L_18;
		L_18 = ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5(L_17, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19;
		L_19 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_19, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_20, /*hidden argument*/NULL);
		// velocity.x = velocity.y * Random.Range(-distanceToLeft, distanceToRight) / -(viewBorders.borderTop() * 2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22 = V_0;
		float L_23 = L_22.get_y_3();
		float L_24 = V_1;
		float L_25 = V_2;
		float L_26;
		L_26 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_24)), L_25, /*hidden argument*/NULL);
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_27 = __this->get_viewBorders_5();
		float L_28;
		L_28 = ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5(L_27, /*hidden argument*/NULL);
		(&V_0)->set_x_2(((float)((float)((float)il2cpp_codegen_multiply((float)L_23, (float)L_26))/(float)((-((float)il2cpp_codegen_multiply((float)L_28, (float)(2.0f))))))));
		// gameObject.GetComponent<Rigidbody2D>().velocity = velocity;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_29;
		L_29 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_30;
		L_30 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_29, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_32;
		L_32 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_31, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_30, L_32, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoveMeteor::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveMeteor_Update_m01742ABCA5719175AD642EB4C69A19AC2F343F24 (MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Vector3 position = gameObject.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_0, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		// if (position.y > viewBorders.borderTop() + size.y || viewBorders.isOutFromBottom(gameObject) || viewBorders.isOutFromLeft(gameObject) ||viewBorders.isOutFromRight(gameObject)) {
		float L_3 = L_2.get_y_3();
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_4 = __this->get_viewBorders_5();
		float L_5;
		L_5 = ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_6 = __this->get_address_of_size_6();
		float L_7 = L_6->get_y_3();
		if ((((float)L_3) > ((float)((float)il2cpp_codegen_add((float)L_5, (float)L_7)))))
		{
			goto IL_0067;
		}
	}
	{
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_8 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_10;
		L_10 = ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622(L_8, L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0067;
		}
	}
	{
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_11 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12;
		L_12 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_13;
		L_13 = ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC(L_11, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0067;
		}
	}
	{
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_14 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_16;
		L_16 = ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2(L_14, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0072;
		}
	}

IL_0067:
	{
		// Destroy(gameObject);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_17, /*hidden argument*/NULL);
	}

IL_0072:
	{
		// }
		return;
	}
}
// System.Void MoveMeteor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveMeteor__ctor_mAA0099A3BE88B0C4D442F25889548CB76968CC9F (MoveMeteor_t3AB90FC261FA616538AF78E41C51D65679FACA98 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoveShip::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveShip_Start_m633249C1BDACD931611617D01EF31FE21B804819 (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// viewBorders = Camera.main.GetComponent<ViewBorders>();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_1;
		L_1 = Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548(L_0, /*hidden argument*/Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		__this->set_viewBorders_5(L_1);
		// input = new Vector2(0, 0);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_input_6(L_2);
		// }
		return;
	}
}
// System.Void MoveShip::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveShip_Update_mA926EB5714CE554E6AD2264E888E44D8756E30A2 (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// input.x = Input.GetAxis("Horizontal");
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_input_6();
		float L_1;
		L_1 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		L_0->set_x_0(L_1);
		// input.y = Input.GetAxis("Vertical");
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_input_6();
		float L_3;
		L_3 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, /*hidden argument*/NULL);
		L_2->set_y_1(L_3);
		// gameObject.GetComponent<Rigidbody2D>().velocity = input * speed;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_5;
		L_5 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_4, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = __this->get_input_6();
		float L_7 = __this->get_speed_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_6, L_7, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_5, L_8, /*hidden argument*/NULL);
		// Vector3 size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9;
		L_9 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_10;
		L_10 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_11;
		L_11 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_12;
		// Vector3 position = gameObject.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_13, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		// if (viewBorders.isOnBorderTop(gameObject))        { position.y = viewBorders.borderTop()    - size.y/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_16 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17;
		L_17 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_18;
		L_18 = ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314(L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00a7;
		}
	}
	{
		// if (viewBorders.isOnBorderTop(gameObject))        { position.y = viewBorders.borderTop()    - size.y/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_19 = __this->get_viewBorders_5();
		float L_20;
		L_20 = ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5(L_19, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = V_0;
		float L_22 = L_21.get_y_3();
		(&V_1)->set_y_3(((float)il2cpp_codegen_subtract((float)L_20, (float)((float)((float)L_22/(float)(2.0f))))));
	}

IL_00a7:
	{
		// if (viewBorders.isOnBorderBottom(gameObject))    { position.y = viewBorders.borderBottom()+ size.y/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_23 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_24;
		L_24 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_25;
		L_25 = ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB(L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00d9;
		}
	}
	{
		// if (viewBorders.isOnBorderBottom(gameObject))    { position.y = viewBorders.borderBottom()+ size.y/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_26 = __this->get_viewBorders_5();
		float L_27;
		L_27 = ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6(L_26, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = V_0;
		float L_29 = L_28.get_y_3();
		(&V_1)->set_y_3(((float)il2cpp_codegen_add((float)L_27, (float)((float)((float)L_29/(float)(2.0f))))));
	}

IL_00d9:
	{
		// if (viewBorders.isOnBorderLeft(gameObject))        { position.x = viewBorders.borderLeft()    + size.x/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_30 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31;
		L_31 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_32;
		L_32 = ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9(L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_010b;
		}
	}
	{
		// if (viewBorders.isOnBorderLeft(gameObject))        { position.x = viewBorders.borderLeft()    + size.x/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_33 = __this->get_viewBorders_5();
		float L_34;
		L_34 = ViewBorders_borderLeft_m7C50A44636CE5AEEC9BE685BF200E100A5CA02C1(L_33, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = V_0;
		float L_36 = L_35.get_x_2();
		(&V_1)->set_x_2(((float)il2cpp_codegen_add((float)L_34, (float)((float)((float)L_36/(float)(2.0f))))));
	}

IL_010b:
	{
		// if (viewBorders.isOnBorderRight(gameObject))     { position.x = viewBorders.borderRight()    - size.x/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_37 = __this->get_viewBorders_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_38;
		L_38 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		bool L_39;
		L_39 = ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5(L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_013d;
		}
	}
	{
		// if (viewBorders.isOnBorderRight(gameObject))     { position.x = viewBorders.borderRight()    - size.x/2; }
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_40 = __this->get_viewBorders_5();
		float L_41;
		L_41 = ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B(L_40, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_42 = V_0;
		float L_43 = L_42.get_x_2();
		(&V_1)->set_x_2(((float)il2cpp_codegen_subtract((float)L_41, (float)((float)((float)L_43/(float)(2.0f))))));
	}

IL_013d:
	{
		// gameObject.GetComponent<Rigidbody2D>().position = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_44;
		L_44 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_45;
		L_45 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_44, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_47;
		L_47 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_46, /*hidden argument*/NULL);
		Rigidbody2D_set_position_m1604084713EB195D44B8B411D4BCAFA5941E3413(L_45, L_47, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Vector2 MoveShip::getInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749 (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, const RuntimeMethod* method)
{
	{
		// return input;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_input_6();
		return L_0;
	}
}
// System.Void MoveShip::OnCollisionEnter2D(UnityEngine.Collision2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveShip_OnCollisionEnter2D_m49C54471E2C7F1AD2780BECFCD186FA8E322CFEC (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral19E30120BF148B4BD74A0C6CE7F85A461E68A628);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2AD47C03F7A83F82E3B2ADFE8A60F1727FD3BEFD);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_0 = NULL;
	{
		// GameObject other = collision.gameObject;
		Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * L_0 = ___collision0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Collision2D_get_gameObject_m6F07B9CA1FAD187933EB6D8E44BD9F870658F89F(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (other.GetComponent<ManageAttributes>().attackPoints > 0) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = V_0;
		ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * L_3;
		L_3 = GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8(L_2, /*hidden argument*/GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		int32_t L_4 = L_3->get_attackPoints_5();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_00a0;
		}
	}
	{
		// gameObject.GetComponent<Animator>().SetTrigger("start");
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_6;
		L_6 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_5, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_6, _stringLiteral2AD47C03F7A83F82E3B2ADFE8A60F1727FD3BEFD, /*hidden argument*/NULL);
		// if (gameObject.GetComponent<ManageAttributes>().AttackedBy(other) <= 0) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * L_8;
		L_8 = GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8(L_7, /*hidden argument*/GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = V_0;
		int32_t L_10;
		L_10 = ManageAttributes_AttackedBy_m5430F87B7648BB5155625697C16E570B564F24D9(L_8, L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_00a0;
		}
	}
	{
		// ManageLifeBar.Instance.RemoveToLife(1);
		ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * L_11 = ((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->get_Instance_4();
		ManageLifeBar_RemoveToLife_mD92710734AE395928F87BFE55661FB8AA6A1368D(L_11, 1, /*hidden argument*/NULL);
		// if (ManageLifeBar.Instance.GetLife() == 1) { gameObject.GetComponent<Animator>().SetTrigger("start"); }
		ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * L_12 = ((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->get_Instance_4();
		int32_t L_13;
		L_13 = ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8_inline(L_12, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_006e;
		}
	}
	{
		// if (ManageLifeBar.Instance.GetLife() == 1) { gameObject.GetComponent<Animator>().SetTrigger("start"); }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_15;
		L_15 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_14, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_15, _stringLiteral2AD47C03F7A83F82E3B2ADFE8A60F1727FD3BEFD, /*hidden argument*/NULL);
	}

IL_006e:
	{
		// if (ManageLifeBar.Instance.GetLife() > 0) { gameObject.GetComponent<ManageAttributes>().RestartLife(); }
		ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * L_16 = ((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->get_Instance_4();
		int32_t L_17;
		L_17 = ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8_inline(L_16, /*hidden argument*/NULL);
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_008c;
		}
	}
	{
		// if (ManageLifeBar.Instance.GetLife() > 0) { gameObject.GetComponent<ManageAttributes>().RestartLife(); }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18;
		L_18 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * L_19;
		L_19 = GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8(L_18, /*hidden argument*/GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		ManageAttributes_RestartLife_mE1F2FB13FDD3F458547FF98DB025C247C114CD43(L_19, /*hidden argument*/NULL);
		// if (ManageLifeBar.Instance.GetLife() > 0) { gameObject.GetComponent<ManageAttributes>().RestartLife(); }
		return;
	}

IL_008c:
	{
		// Debug.Log("GAME OVER");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(_stringLiteral19E30120BF148B4BD74A0C6CE7F85A461E68A628, /*hidden argument*/NULL);
		// ManageGame.Instance.PauseGameLevel();
		ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0 * L_20 = ((ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_StaticFields*)il2cpp_codegen_static_fields_for(ManageGame_t2C218A9DE1F14F8B913C5E6BDBF42873FD4B5AE0_il2cpp_TypeInfo_var))->get_Instance_4();
		ManageGame_PauseGameLevel_m39DE45CF5CDBD0EB1851911801337EE3DC6F9DF4(L_20, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		// }
		return;
	}
}
// System.Void MoveShip::OnCollisionExit2D(UnityEngine.Collision2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveShip_OnCollisionExit2D_m86C77026B656E4D502D73BC55F0E946A4F3F92EE (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, Collision2D_t95B5FD331CE95276D3658140844190B485D26564 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (ManageLifeBar.Instance.GetLife() != 1) {
		ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * L_0 = ((ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_StaticFields*)il2cpp_codegen_static_fields_for(ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57_il2cpp_TypeInfo_var))->get_Instance_4();
		int32_t L_1;
		L_1 = ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8_inline(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		// gameObject.GetComponent<Animator>().SetTrigger("stop");
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3;
		L_3 = GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36(L_2, /*hidden argument*/GameObject_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m7B3DD04368DD8D8896F3694FFDF28D1CE00F5B36_RuntimeMethod_var);
		Animator_SetTrigger_m2D79D155CABD81B1CC75EFC35D90508B58D7211C(L_3, _stringLiteral7FB065FC47DDCF8134948800A310281E12F058C7, /*hidden argument*/NULL);
	}

IL_0022:
	{
		// }
		return;
	}
}
// System.Void MoveShip::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveShip__ctor_m6DDFD05FA781A523D7E648BAE8669F7836FD8ADA (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PropulsionEffect::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropulsionEffect_Start_m5B5C5CF51642E66D1DA10F9A8814B042D4F73BE4 (PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral491808D7EEB006360E8FF7C51AA2757B43D35E12);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector3 size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_1), /*hidden argument*/NULL);
		V_0 = L_3;
		// fire = new GameObject("fire", typeof(SpriteRenderer));
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_4 = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_5 = L_4;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_6 = { reinterpret_cast<intptr_t> (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7;
		L_7 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_6, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)il2cpp_codegen_object_new(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_il2cpp_TypeInfo_var);
		GameObject__ctor_m9829583AE3BF1285861C580895202F760F3A82E8(L_8, _stringLiteral491808D7EEB006360E8FF7C51AA2757B43D35E12, L_5, /*hidden argument*/NULL);
		__this->set_fire_8(L_8);
		// fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMin;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = __this->get_fire_8();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_10;
		L_10 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_11 = __this->get_spriteSpeedMin_4();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_10, L_11, /*hidden argument*/NULL);
		// fire.GetComponent<Transform>().parent = gameObject.transform;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_fire_8();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_12, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14;
		L_14 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_14, /*hidden argument*/NULL);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_13, L_15, /*hidden argument*/NULL);
		// fire.GetComponent<Transform>().localScale = scale;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = __this->get_fire_8();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_16, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = __this->get_scale_7();
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_17, L_18, /*hidden argument*/NULL);
		// fire.GetComponent<Transform>().localPosition = new Vector3(0, -size.y/2, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = __this->get_fire_8();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_19, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = V_0;
		float L_22 = L_21.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), (0.0f), ((float)((float)((-L_22))/(float)(2.0f))), (0.0f), /*hidden argument*/NULL);
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_20, L_23, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PropulsionEffect::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropulsionEffect_Update_m1F2A89E9B46D24D010D6F7A150193A4304A29145 (PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisMoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A_m9535F8179E6E9064B77556325A1AED291369EB53_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  G_B2_0;
	memset((&G_B2_0), 0, sizeof(G_B2_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  G_B1_0;
	memset((&G_B1_0), 0, sizeof(G_B1_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  G_B6_0;
	memset((&G_B6_0), 0, sizeof(G_B6_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  G_B5_0;
	memset((&G_B5_0), 0, sizeof(G_B5_0));
	{
		// Vector2 input = gameObject.GetComponent<MoveShip>().getInput();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * L_1;
		L_1 = GameObject_GetComponent_TisMoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A_m9535F8179E6E9064B77556325A1AED291369EB53(L_0, /*hidden argument*/GameObject_GetComponent_TisMoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A_m9535F8179E6E9064B77556325A1AED291369EB53_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		L_2 = MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749_inline(L_1, /*hidden argument*/NULL);
		// if (input.y < 0) { fire.GetComponent<SpriteRenderer>().sprite = null; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = L_2;
		float L_4 = L_3.get_y_1();
		G_B1_0 = L_3;
		if ((!(((float)L_4) < ((float)(0.0f)))))
		{
			G_B2_0 = L_3;
			goto IL_002e;
		}
	}
	{
		// if (input.y < 0) { fire.GetComponent<SpriteRenderer>().sprite = null; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_fire_8();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_6;
		L_6 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_5, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_6, (Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 *)NULL, /*hidden argument*/NULL);
		G_B2_0 = G_B1_0;
	}

IL_002e:
	{
		// if (input.y >= 0) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMin; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = G_B2_0;
		float L_8 = L_7.get_y_1();
		G_B3_0 = L_7;
		if ((!(((float)L_8) >= ((float)(0.0f)))))
		{
			G_B4_0 = L_7;
			goto IL_0051;
		}
	}
	{
		// if (input.y >= 0) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMin; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_9 = __this->get_fire_8();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_10;
		L_10 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_11 = __this->get_spriteSpeedMin_4();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_10, L_11, /*hidden argument*/NULL);
		G_B4_0 = G_B3_0;
	}

IL_0051:
	{
		// if (input.y > 0.5) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMedium; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = G_B4_0;
		float L_13 = L_12.get_y_1();
		G_B5_0 = L_12;
		if ((!(((double)((double)((double)L_13))) > ((double)(0.5)))))
		{
			G_B6_0 = L_12;
			goto IL_0079;
		}
	}
	{
		// if (input.y > 0.5) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMedium; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = __this->get_fire_8();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_15;
		L_15 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_14, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_16 = __this->get_spriteSpeedMedium_5();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_15, L_16, /*hidden argument*/NULL);
		G_B6_0 = G_B5_0;
	}

IL_0079:
	{
		// if (input.y == 1) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMax; }
		float L_17 = G_B6_0.get_y_1();
		if ((!(((float)L_17) == ((float)(1.0f)))))
		{
			goto IL_009b;
		}
	}
	{
		// if (input.y == 1) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMax; }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = __this->get_fire_8();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_19;
		L_19 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_18, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_20 = __this->get_spriteSpeedMax_6();
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_19, L_20, /*hidden argument*/NULL);
	}

IL_009b:
	{
		// }
		return;
	}
}
// System.Void PropulsionEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PropulsionEffect__ctor_mA7C23DCB8BB410B81AE781CD1A4A56161ECE8FC5 (PropulsionEffect_t0AB8B0F9535A19C9DD1CA959DB93F90E45F8BE17 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ScrollBackground::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScrollBackground_Start_m6C6E6A47F1F0915E5ADF9733EA33BB8CAA5D558A (ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// viewBorders = Camera.main.GetComponent<ViewBorders>();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_1;
		L_1 = Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548(L_0, /*hidden argument*/Component_GetComponent_TisViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C_mDCD3B6AD0E67C0D16E01C080B0086FB19BA89548_RuntimeMethod_var);
		__this->set_viewBorders_8(L_1);
		// sizeY = background1.GetComponent<SpriteRenderer>().bounds.size.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_background1_4();
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_3;
		L_3 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_2, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_4;
		L_4 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), /*hidden argument*/NULL);
		float L_6 = L_5.get_y_3();
		__this->set_sizeY_9(L_6);
		// background1.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -speedY, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_background1_4();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_8;
		L_8 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_7, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		float L_9 = __this->get_speedY_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_10), (0.0f), ((-L_9)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11;
		L_11 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_10, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_8, L_11, /*hidden argument*/NULL);
		// background2.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -speedY, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_background2_5();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_13;
		L_13 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_12, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		float L_14 = __this->get_speedY_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_15), (0.0f), ((-L_14)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_16;
		L_16 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_15, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_13, L_16, /*hidden argument*/NULL);
		// background3.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -speedY, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = __this->get_background3_6();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_18;
		L_18 = GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40(L_17, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_mE749A8DAAB8733CB623A476DC361B88581AF3E40_RuntimeMethod_var);
		float L_19 = __this->get_speedY_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_20), (0.0f), ((-L_19)), (0.0f), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21;
		L_21 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_20, /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_18, L_21, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ScrollBackground::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScrollBackground_Update_m00D44D6E0B21CF0DFAE601DC0EB6FA2B0A2EE237 (ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (background1.GetComponent<Transform>().position.y + (sizeY/2) < viewBorders.borderBottom()) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_0, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_y_3();
		float L_4 = __this->get_sizeY_9();
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_5 = __this->get_viewBorders_8();
		float L_6;
		L_6 = ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6(L_5, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_3, (float)((float)((float)L_4/(float)(2.0f)))))) < ((float)L_6))))
		{
			goto IL_007a;
		}
	}
	{
		// Vector3 position = background1.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_7, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		// position.y = background3.GetComponent<Transform>().position.y + (sizeY/2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_10, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_11, /*hidden argument*/NULL);
		float L_13 = L_12.get_y_3();
		float L_14 = __this->get_sizeY_9();
		(&V_0)->set_y_3(((float)il2cpp_codegen_add((float)L_13, (float)((float)((float)L_14/(float)(2.0f))))));
		// background1.GetComponent<Transform>().position = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_15, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = V_0;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_16, L_17, /*hidden argument*/NULL);
	}

IL_007a:
	{
		// if (background2.GetComponent<Transform>().position.y + (sizeY/2) < viewBorders.borderBottom()) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_18, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_19, /*hidden argument*/NULL);
		float L_21 = L_20.get_y_3();
		float L_22 = __this->get_sizeY_9();
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_23 = __this->get_viewBorders_8();
		float L_24;
		L_24 = ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6(L_23, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_21, (float)((float)((float)L_22/(float)(2.0f)))))) < ((float)L_24))))
		{
			goto IL_00f4;
		}
	}
	{
		// Vector3 position = background2.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26;
		L_26 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_25, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		// position.y = background1.GetComponent<Transform>().position.y + (sizeY/2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = __this->get_background1_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_29;
		L_29 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_28, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_29, /*hidden argument*/NULL);
		float L_31 = L_30.get_y_3();
		float L_32 = __this->get_sizeY_9();
		(&V_1)->set_y_3(((float)il2cpp_codegen_add((float)L_31, (float)((float)((float)L_32/(float)(2.0f))))));
		// background2.GetComponent<Transform>().position = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_33 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34;
		L_34 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_33, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = V_1;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_34, L_35, /*hidden argument*/NULL);
	}

IL_00f4:
	{
		// if (background3.GetComponent<Transform>().position.y + (sizeY/2) < viewBorders.borderBottom()) {
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_36 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_37;
		L_37 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_36, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_37, /*hidden argument*/NULL);
		float L_39 = L_38.get_y_3();
		float L_40 = __this->get_sizeY_9();
		ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * L_41 = __this->get_viewBorders_8();
		float L_42;
		L_42 = ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6(L_41, /*hidden argument*/NULL);
		if ((!(((float)((float)il2cpp_codegen_add((float)L_39, (float)((float)((float)L_40/(float)(2.0f)))))) < ((float)L_42))))
		{
			goto IL_016e;
		}
	}
	{
		// Vector3 position = background3.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_43 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_44;
		L_44 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_43, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45;
		L_45 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_44, /*hidden argument*/NULL);
		V_2 = L_45;
		// position.y = background2.GetComponent<Transform>().position.y + (sizeY/2);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_46 = __this->get_background2_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_47;
		L_47 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_46, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_48;
		L_48 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_47, /*hidden argument*/NULL);
		float L_49 = L_48.get_y_3();
		float L_50 = __this->get_sizeY_9();
		(&V_2)->set_y_3(((float)il2cpp_codegen_add((float)L_49, (float)((float)((float)L_50/(float)(2.0f))))));
		// background3.GetComponent<Transform>().position = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_51 = __this->get_background3_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_52;
		L_52 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_51, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53 = V_2;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_52, L_53, /*hidden argument*/NULL);
	}

IL_016e:
	{
		// }
		return;
	}
}
// System.Void ScrollBackground::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScrollBackground__ctor_mF2E8B5FA4341B1B06A501DC440FBC8B5BAE29380 (ScrollBackground_t58555F875AA5982FCA2D8F06C34A25F023CC4951 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ShootLaser1::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShootLaser1_Update_mA57094A89B4E1DFCCBB74B3FDD62A63F8A38AD35 (ShootLaser1_t87558B0D9E149549A87216AB43D82F10C0AC0896 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// if (Input.GetKeyDown(KeyCode.Space)) {
		bool L_0;
		L_0 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)32), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00c2;
		}
	}
	{
		// Vector3 shipSize = gameObject.GetComponent<SpriteRenderer>().bounds.size;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_2;
		L_2 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_1, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_3;
		L_3 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_2, /*hidden argument*/NULL);
		V_4 = L_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_4), /*hidden argument*/NULL);
		V_0 = L_4;
		// Vector3 shipPosition = gameObject.GetComponent<Transform>().position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
		L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		// Vector3 laserSize = laser.GetComponent<Transform>().GetChild(0).gameObject.GetComponent<SpriteRenderer>().bounds.size;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = __this->get_laser_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_8, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_9, 0, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
		L_11 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_10, /*hidden argument*/NULL);
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_12;
		L_12 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_11, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_13;
		L_13 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_4), /*hidden argument*/NULL);
		V_2 = L_14;
		// Vector3 laserPosition = new Vector3(shipPosition.x, shipPosition.y + laserSize.y/2 + shipSize.y/2 + 0.1f, shipPosition.z);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = V_1;
		float L_16 = L_15.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17 = V_1;
		float L_18 = L_17.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = V_2;
		float L_20 = L_19.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = V_0;
		float L_22 = L_21.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = V_1;
		float L_24 = L_23.get_z_4();
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), L_16, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_18, (float)((float)((float)L_20/(float)(2.0f))))), (float)((float)((float)L_22/(float)(2.0f))))), (float)(0.100000001f))), L_24, /*hidden argument*/NULL);
		// laser.GetComponent<ManageAttributes>().belongToThePlayer = fromPlayer;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = __this->get_laser_4();
		ManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9 * L_26;
		L_26 = GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8(L_25, /*hidden argument*/GameObject_GetComponent_TisManageAttributes_tDEC85DA73611737765DE5B74FF9080521CA1D9E9_m0BAE43A141C8BD456A2651719A3AF68BD30E26A8_RuntimeMethod_var);
		bool L_27 = __this->get_fromPlayer_5();
		L_26->set_belongToThePlayer_7(L_27);
		// Instantiate(laser, laserPosition, Quaternion.identity);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = __this->get_laser_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = V_3;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_30;
		L_30 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31;
		L_31 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_28, L_29, L_30, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
	}

IL_00c2:
	{
		// }
		return;
	}
}
// System.Void ShootLaser1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ShootLaser1__ctor_mA41F457EBC289D3DB39825F1C7ADCC529437630E (ShootLaser1_t87558B0D9E149549A87216AB43D82F10C0AC0896 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ViewBorders::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ViewBorders_Start_m29C7D0AB22F9C7863C2A48AA568D023FA069CC83 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method)
{
	{
		// cameraBottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_1), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Camera_ViewportToWorldPoint_m1273EE3868551C6FF551ABA9A76DC7D66E883116(L_0, L_1, /*hidden argument*/NULL);
		__this->set_cameraBottomLeft_4(L_2);
		// cameraTopRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Camera_ViewportToWorldPoint_m1273EE3868551C6FF551ABA9A76DC7D66E883116(L_3, L_4, /*hidden argument*/NULL);
		__this->set_cameraTopRight_5(L_5);
		// }
		return;
	}
}
// System.Single ViewBorders::borderTop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderTop_m05408569B69421F8B5A90B06443600F3CBE67DC5 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method)
{
	{
		// public float borderTop() { return cameraTopRight.y; }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_cameraTopRight_5();
		float L_1 = L_0->get_y_3();
		return L_1;
	}
}
// System.Single ViewBorders::borderBottom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderBottom_m137EB04C6FA082811BC757895A9396BD3BDFADE6 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method)
{
	{
		// public float borderBottom() { return cameraBottomLeft.y; }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_cameraBottomLeft_4();
		float L_1 = L_0->get_y_3();
		return L_1;
	}
}
// System.Single ViewBorders::borderLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderLeft_m7C50A44636CE5AEEC9BE685BF200E100A5CA02C1 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method)
{
	{
		// public float borderLeft() { return cameraBottomLeft.x; }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_cameraBottomLeft_4();
		float L_1 = L_0->get_x_2();
		return L_1;
	}
}
// System.Single ViewBorders::borderRight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ViewBorders_borderRight_mD304C2A248F748EF963F074407B06C6289A8451B (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method)
{
	{
		// public float borderRight() { return cameraTopRight.x; }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_cameraTopRight_5();
		float L_1 = L_0->get_x_2();
		return L_1;
	}
}
// System.Boolean ViewBorders::isOnBorder(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorder_m05F87216D6551D8CC695E4B856C9FB201D1C4E26 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	{
		// return isOnBorderTop(object2D) || isOnBorderBottom(object2D) || isOnBorderLeft(object2D) || isOnBorderRight(object2D);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		bool L_1;
		L_1 = ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = ___object2D0;
		bool L_3;
		L_3 = ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB(__this, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___object2D0;
		bool L_5;
		L_5 = ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9(__this, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = ___object2D0;
		bool L_7;
		L_7 = ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5(__this, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0023:
	{
		return (bool)1;
	}
}
// System.Boolean ViewBorders::isOut(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOut_m0A983F94132A2262E813A7FD17DDC6B666A5AC3C (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	{
		// return isOutFromTop(object2D) || isOutFromBottom(object2D) || isOutFromLeft(object2D) || isOutFromRight(object2D);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		bool L_1;
		L_1 = ViewBorders_isOutFromTop_mE6047837B4FFA21B6B70486AF6821944D9A44EAA(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = ___object2D0;
		bool L_3;
		L_3 = ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622(__this, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___object2D0;
		bool L_5;
		L_5 = ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC(__this, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0023;
		}
	}
	{
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = ___object2D0;
		bool L_7;
		L_7 = ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2(__this, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0023:
	{
		return (bool)1;
	}
}
// System.Boolean ViewBorders::isOnBorderTop(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderTop_mC7A07937B2924F325353F09B1F5A36B41E0CF314 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float sizeY = object2D.GetComponent<SpriteRenderer>().bounds.size.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_2), /*hidden argument*/NULL);
		float L_4 = L_3.get_y_3();
		V_0 = L_4;
		// float positionY = object2D.GetComponent<Transform>().position.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_y_3();
		V_1 = L_8;
		// return positionY + (sizeY/2) > cameraTopRight.y && positionY - (sizeY/2) < cameraTopRight.y;
		float L_9 = V_1;
		float L_10 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = __this->get_address_of_cameraTopRight_5();
		float L_12 = L_11->get_y_3();
		if ((!(((float)((float)il2cpp_codegen_add((float)L_9, (float)((float)((float)L_10/(float)(2.0f)))))) > ((float)L_12))))
		{
			goto IL_0057;
		}
	}
	{
		float L_13 = V_1;
		float L_14 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_15 = __this->get_address_of_cameraTopRight_5();
		float L_16 = L_15->get_y_3();
		return (bool)((((float)((float)il2cpp_codegen_subtract((float)L_13, (float)((float)((float)L_14/(float)(2.0f)))))) < ((float)L_16))? 1 : 0);
	}

IL_0057:
	{
		return (bool)0;
	}
}
// System.Boolean ViewBorders::isOnBorderBottom(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderBottom_mECEF8198047324612ED32D8D183FE02530721FDB (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float sizeY = object2D.GetComponent<SpriteRenderer>().bounds.size.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_2), /*hidden argument*/NULL);
		float L_4 = L_3.get_y_3();
		V_0 = L_4;
		// float positionY = object2D.GetComponent<Transform>().position.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_y_3();
		V_1 = L_8;
		// return positionY - (sizeY/2) < cameraBottomLeft.y && positionY + (sizeY/2) > cameraBottomLeft.y;
		float L_9 = V_1;
		float L_10 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = __this->get_address_of_cameraBottomLeft_4();
		float L_12 = L_11->get_y_3();
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_9, (float)((float)((float)L_10/(float)(2.0f)))))) < ((float)L_12))))
		{
			goto IL_0057;
		}
	}
	{
		float L_13 = V_1;
		float L_14 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_15 = __this->get_address_of_cameraBottomLeft_4();
		float L_16 = L_15->get_y_3();
		return (bool)((((float)((float)il2cpp_codegen_add((float)L_13, (float)((float)((float)L_14/(float)(2.0f)))))) > ((float)L_16))? 1 : 0);
	}

IL_0057:
	{
		return (bool)0;
	}
}
// System.Boolean ViewBorders::isOnBorderLeft(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderLeft_mE7F38D4B1439B1ABDC65F7BF60AE6ED1AFE1B2C9 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float sizeX = object2D.GetComponent<SpriteRenderer>().bounds.size.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_2), /*hidden argument*/NULL);
		float L_4 = L_3.get_x_2();
		V_0 = L_4;
		// float positionX = object2D.GetComponent<Transform>().position.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		V_1 = L_8;
		// return positionX - (sizeX/2) < cameraBottomLeft.x && positionX + (sizeX/2) > cameraBottomLeft.x;
		float L_9 = V_1;
		float L_10 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = __this->get_address_of_cameraBottomLeft_4();
		float L_12 = L_11->get_x_2();
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_9, (float)((float)((float)L_10/(float)(2.0f)))))) < ((float)L_12))))
		{
			goto IL_0057;
		}
	}
	{
		float L_13 = V_1;
		float L_14 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_15 = __this->get_address_of_cameraBottomLeft_4();
		float L_16 = L_15->get_x_2();
		return (bool)((((float)((float)il2cpp_codegen_add((float)L_13, (float)((float)((float)L_14/(float)(2.0f)))))) > ((float)L_16))? 1 : 0);
	}

IL_0057:
	{
		return (bool)0;
	}
}
// System.Boolean ViewBorders::isOnBorderRight(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOnBorderRight_m7C1672EE2AF5FE43E126BA99E8397FD1C8FB44F5 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float sizeX = object2D.GetComponent<SpriteRenderer>().bounds.size.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_2), /*hidden argument*/NULL);
		float L_4 = L_3.get_x_2();
		V_0 = L_4;
		// float positionX = object2D.GetComponent<Transform>().position.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		V_1 = L_8;
		// return positionX + (sizeX/2) > cameraTopRight.x && positionX - (sizeX/2) < cameraTopRight.x;
		float L_9 = V_1;
		float L_10 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = __this->get_address_of_cameraTopRight_5();
		float L_12 = L_11->get_x_2();
		if ((!(((float)((float)il2cpp_codegen_add((float)L_9, (float)((float)((float)L_10/(float)(2.0f)))))) > ((float)L_12))))
		{
			goto IL_0057;
		}
	}
	{
		float L_13 = V_1;
		float L_14 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_15 = __this->get_address_of_cameraTopRight_5();
		float L_16 = L_15->get_x_2();
		return (bool)((((float)((float)il2cpp_codegen_subtract((float)L_13, (float)((float)((float)L_14/(float)(2.0f)))))) < ((float)L_16))? 1 : 0);
	}

IL_0057:
	{
		return (bool)0;
	}
}
// System.Boolean ViewBorders::isOutFromTop(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromTop_mE6047837B4FFA21B6B70486AF6821944D9A44EAA (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// float sizeY = object2D.GetComponent<SpriteRenderer>().bounds.size.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_1), /*hidden argument*/NULL);
		float L_4 = L_3.get_y_3();
		V_0 = L_4;
		// float positionY = object2D.GetComponent<Transform>().position.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_y_3();
		// return positionY - (sizeY/2) > cameraTopRight.y;
		float L_9 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_10 = __this->get_address_of_cameraTopRight_5();
		float L_11 = L_10->get_y_3();
		return (bool)((((float)((float)il2cpp_codegen_subtract((float)L_8, (float)((float)((float)L_9/(float)(2.0f)))))) > ((float)L_11))? 1 : 0);
	}
}
// System.Boolean ViewBorders::isOutFromBottom(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromBottom_mFC5C92E1010F59E42AF6D140081AC9C7D3453622 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// float sizeY = object2D.GetComponent<SpriteRenderer>().bounds.size.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_1), /*hidden argument*/NULL);
		float L_4 = L_3.get_y_3();
		V_0 = L_4;
		// float positionY = object2D.GetComponent<Transform>().position.y;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_y_3();
		// return positionY + (sizeY/2) < cameraBottomLeft.y;
		float L_9 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_10 = __this->get_address_of_cameraBottomLeft_4();
		float L_11 = L_10->get_y_3();
		return (bool)((((float)((float)il2cpp_codegen_add((float)L_8, (float)((float)((float)L_9/(float)(2.0f)))))) < ((float)L_11))? 1 : 0);
	}
}
// System.Boolean ViewBorders::isOutFromLeft(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromLeft_m3671C8DA01F15A9EC4505C41A69881BD7A6953EC (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// float sizeX = object2D.GetComponent<SpriteRenderer>().bounds.size.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_1), /*hidden argument*/NULL);
		float L_4 = L_3.get_x_2();
		V_0 = L_4;
		// float positionX = object2D.GetComponent<Transform>().position.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		// return positionX + (sizeX/2) < cameraBottomLeft.x;
		float L_9 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_10 = __this->get_address_of_cameraBottomLeft_4();
		float L_11 = L_10->get_x_2();
		return (bool)((((float)((float)il2cpp_codegen_add((float)L_8, (float)((float)((float)L_9/(float)(2.0f)))))) < ((float)L_11))? 1 : 0);
	}
}
// System.Boolean ViewBorders::isOutFromRight(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ViewBorders_isOutFromRight_m6219CE0239CF054D6404DD8913051380AB9EE0F2 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___object2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// float sizeX = object2D.GetComponent<SpriteRenderer>().bounds.size.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = ___object2D0;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_1;
		L_1 = GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m137AC519978188EDF693EDACB218EFF6F37078C1_RuntimeMethod_var);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_2;
		L_2 = Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_1), /*hidden argument*/NULL);
		float L_4 = L_3.get_x_2();
		V_0 = L_4;
		// float positionX = object2D.GetComponent<Transform>().position.x;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___object2D0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228(L_5, /*hidden argument*/GameObject_GetComponent_TisTransform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_m60FB1B4E281B360A56A54509EBA605FD5F04D228_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		float L_8 = L_7.get_x_2();
		// return positionX - (sizeX/2) > cameraTopRight.x;
		float L_9 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_10 = __this->get_address_of_cameraTopRight_5();
		float L_11 = L_10->get_x_2();
		return (bool)((((float)((float)il2cpp_codegen_subtract((float)L_8, (float)((float)((float)L_9/(float)(2.0f)))))) > ((float)L_11))? 1 : 0);
	}
}
// System.Void ViewBorders::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ViewBorders__ctor_m7B2ADBB9D3D1CCBF60F565A10813D46D95F50561 (ViewBorders_tB676A3ACB0F9EF311C9A73FC81237EE8FC4AD12C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ManageLifeBar_GetLife_mC9E4FEDD550ACDD7C32BF0DE53582731935310D8_inline (ManageLifeBar_tED7E0D0B2F0BADD871D4A97E5686812F4C10AD57 * __this, const RuntimeMethod* method)
{
	{
		// return currentLife;
		int32_t L_0 = __this->get_currentLife_10();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  MoveShip_getInput_mCA68AE3D542409357BCE81D126E564E611C1C749_inline (MoveShip_tFD370526A7DF114755FE6854AEB77BECD4CC4F5A * __this, const RuntimeMethod* method)
{
	{
		// return input;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_input_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m7FA90926D9267868473EF90941F6BF794EC87FF2_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Item_m730FCAD2646FA94B07D1216A512B09AB9F0BBA5D_gshared_inline (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get__items_1();
		int32_t L_3 = ___index0;
		int32_t L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)L_2, (int32_t)L_3);
		return (int32_t)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return (int32_t)L_0;
	}
}
