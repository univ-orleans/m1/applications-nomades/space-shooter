﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[AddComponentMenu("Scripts/UI/Manager Life Bar")]
public class ManageLifeBar : MonoBehaviour {

	public int initLife;
	public TMP_Text counter;
	private int currentLife;


	private void Start() {
		currentLife = initLife;
	}


	private void Update() {		
		counter.text = currentLife.ToString();
	}


	public void AddToLife(int toAdd) {
		currentLife += toAdd;
	}


	public void RemoveToLife(int toRemove) {
		currentLife -= toRemove;
		if (currentLife < 0) { currentLife = 0; }
	}

	public int GetLife() {
		return currentLife;
	}
}
