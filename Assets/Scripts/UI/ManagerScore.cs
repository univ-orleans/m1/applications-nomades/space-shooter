﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[AddComponentMenu("Scripts/UI/Manager Score Counter")]
public class ManagerScore : MonoBehaviour {

	private static ManagerScore _instance;
	public int initScore;
	public TMP_Text counter;
	private int score;


	public static ManagerScore Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<ManagerScore>();
				if (_instance == null) {
					_instance = new GameObject().AddComponent<ManagerScore>();
				}
			}
			return _instance;
		}
	}

	private void Awake() {
		if (_instance == null) { _instance = this; }
		else { Destroy(this); }
	}


	private void Update() {
		counter.text = score.ToString();
	}


	public void AddToScore(int toAdd) {
		score += toAdd;
	}

	public void RemoveToScore(int toRemove) {
		score -= toRemove;
	}

	public int GetScore() {
		return score;
	}
}
