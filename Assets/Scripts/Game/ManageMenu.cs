using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManageMenu : MonoBehaviour {

	public void ShowScene(string sceneName) {
		ManageGame.Instance.ShowScene(sceneName);
	}

	public void PauseGameLevel() {
		ManageGame.Instance.PauseGameLevel();
	}

	public void ContinueGameLevel() {
		ManageGame.Instance.ContinueGameLevel();
	}

	public void StopGameLevel() {
		ManageGame.Instance.StopGameLevel();
	}
}
