﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Game/Manager Attributes")]
public class ManageAttributes : MonoBehaviour {

	public int pointsToWin;
	public int attackPoints;
	public int lifePoints;
	public bool belongToThePlayer;
	private int restOfLifePoints;


	private void Start() {
		restOfLifePoints = lifePoints;
	}

	public int AttackedBy(GameObject other) {
		restOfLifePoints -= other.GetComponent<ManageAttributes>().attackPoints;
		return restOfLifePoints;
	}

	public int getLifePoints() {
		return restOfLifePoints;
	}

	public void RestartLife() {
		restOfLifePoints = lifePoints;
	}

}
