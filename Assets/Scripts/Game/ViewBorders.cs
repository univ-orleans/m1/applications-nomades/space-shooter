﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Game/View Borders")]
public class ViewBorders : MonoBehaviour {

	private static ViewBorders _instance;
	private Vector3 cameraBottomLeft;
	private Vector3 cameraTopRight;


	public static ViewBorders Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<ViewBorders>();
				if (_instance == null) {
					_instance = new GameObject().AddComponent<ViewBorders>();
				}
			}
			return _instance;
		}
	}

	private void Awake() {
		if (_instance == null) { _instance = this; }
		else { Destroy(this); }
	}



	void Start() {
		cameraBottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
		cameraTopRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
	}



	public float borderTop() { return cameraTopRight.y; }
	public float borderBottom() { return cameraBottomLeft.y; }
	public float borderLeft() { return cameraBottomLeft.x; }
	public float borderRight() { return cameraTopRight.x; }
	public float width() { return cameraTopRight.x * 2; }
	public float height() { return cameraTopRight.y * 2; }


	public bool isOnBorder(GameObject object2D) {
		return isOnBorderTop(object2D) || isOnBorderBottom(object2D) || isOnBorderLeft(object2D) || isOnBorderRight(object2D);
	}


	public bool isOut(GameObject object2D) {
		return isOutFromTop(object2D) || isOutFromBottom(object2D) || isOutFromLeft(object2D) || isOutFromRight(object2D);
	}


	public bool isOnBorderTop(GameObject object2D) {
		float sizeY = getSizeY(object2D);
		float positionY = object2D.transform.position.y;
		return positionY + (sizeY/2) > cameraTopRight.y && positionY - (sizeY/2) < cameraTopRight.y;
	}


	public bool isOnBorderBottom(GameObject object2D) {
		float sizeY = getSizeY(object2D);
		float positionY = object2D.transform.position.y;
		return positionY - (sizeY/2) < cameraBottomLeft.y && positionY + (sizeY/2) > cameraBottomLeft.y;
	}


	public bool isOnBorderLeft(GameObject object2D) {
		float sizeX = getSizeX(object2D);
		float positionX = object2D.transform.position.x;
		return positionX - (sizeX/2) < cameraBottomLeft.x && positionX + (sizeX/2) > cameraBottomLeft.x;
	}


	public bool isOnBorderRight(GameObject object2D) {
		float sizeX = getSizeX(object2D);
		float positionX = object2D.transform.position.x;
		return positionX + (sizeX/2) > cameraTopRight.x && positionX - (sizeX/2) < cameraTopRight.x;
	}


	public bool isOutFromTop(GameObject object2D) {
		float sizeY = getSizeY(object2D);
		float positionY = object2D.transform.position.y;
		return positionY - (sizeY/2) > cameraTopRight.y;
	}


	public bool isOutFromBottom(GameObject object2D) {
		float sizeY = getSizeY(object2D);
		float positionY = object2D.transform.position.y;
		return positionY + (sizeY/2) < cameraBottomLeft.y;
	}


	public bool isOutFromLeft(GameObject object2D) {
		float sizeX = getSizeX(object2D);
		float positionX = object2D.transform.position.x;
		return positionX + (sizeX/2) < cameraBottomLeft.x;
	}


	public bool isOutFromRight(GameObject object2D) {
		float sizeX = getSizeX(object2D);
		float positionX = object2D.transform.position.x;
		return positionX - (sizeX/2) > cameraTopRight.x;
	}

	private float getSizeY(GameObject object2D) {
		float sizeY = 0f;
		if (object2D.TryGetComponent<SpriteRenderer>(out SpriteRenderer component1)) { sizeY = component1.bounds.size.y; }
		else if (object2D.TryGetComponent<RectTransform>(out RectTransform component2)) { sizeY = component2.rect.height; }
		return sizeY;
	}

	private float getSizeX(GameObject object2D) {
		float sizeX = 0f;
		if (object2D.TryGetComponent<SpriteRenderer>(out SpriteRenderer component1)) { sizeX = component1.bounds.size.x; }
		else if (object2D.TryGetComponent<RectTransform>(out RectTransform component2)) { sizeX = component2.rect.width; }
		return sizeX;
	}
}
