﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropulsionEffect : MonoBehaviour {

	public Sprite spriteSpeedMin;
	public Sprite spriteSpeedMedium;
	public Sprite spriteSpeedMax;
	public Vector3 scale;
	private GameObject fire;


	private void Start() {
		Vector3 size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
		
		// create fire effect
		fire = new GameObject("fire", typeof(SpriteRenderer));
		fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMin;
		fire.transform.parent = gameObject.transform;
		fire.transform.localScale = scale;
		fire.transform.localPosition = new Vector3(0, -size.y/2, 0);
	}


	private void Update() {
		Vector2 input = gameObject.GetComponent<MoveShip>().getInput();
		if (input.y < 0) { fire.GetComponent<SpriteRenderer>().sprite = null; }
		if (input.y >= 0) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMin; }
		if (input.y > 0.5) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMedium; }
		if (input.y == 1) { fire.GetComponent<SpriteRenderer>().sprite = spriteSpeedMax; }
	}
}
