﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveShip : MonoBehaviour {

	public float speed;
	private ViewBorders viewBorders;
	private Vector2 input;
	public GameObject shield;
	private bool shieldActivate;

	[Header("Mobile command")]
	public GameObject mobileCommands;
	private Joystick joystick;


	private void Start() {
		viewBorders = ViewBorders.Instance;
		input = new Vector2(0, 0);

		// deactivate shield
		shield.SetActive(false);

		joystick = mobileCommands.GetComponentInChildren<Joystick>();
		if (Application.isMobilePlatform) { mobileCommands.SetActive(true); }
		else { mobileCommands.SetActive(false); }
	}


	private void Update() {
		// Move object
		input.x = joystick.Horizontal + Input.GetAxis("Horizontal");
		input.y = joystick.Vertical + Input.GetAxis("Vertical");
		gameObject.GetComponent<Rigidbody2D>().velocity = input * speed;

		// Restrict ship position
		Vector3 size = gameObject.GetComponent<SpriteRenderer>().bounds.size;
		Vector3 position = gameObject.transform.position;
		if (viewBorders.isOnBorderTop(gameObject))		{ position.y = viewBorders.borderTop()	- size.y/2; }
		if (viewBorders.isOnBorderBottom(gameObject))	{ position.y = viewBorders.borderBottom()+ size.y/2; }
		if (viewBorders.isOnBorderLeft(gameObject))		{ position.x = viewBorders.borderLeft()	+ size.x/2; }
		if (viewBorders.isOnBorderRight(gameObject)) 	{ position.x = viewBorders.borderRight()	- size.x/2; }
		gameObject.GetComponent<Rigidbody2D>().position = position;
	}

	public Vector2 getInput() {
		return input;
	}


	private void OnCollisionEnter2D(Collision2D collision) {
		GameObject other = collision.gameObject;

		if (other.GetComponent<ManageAttributes>().attackPoints > 0) {
			if (shieldActivate) {
				// if shield down
				if (shield.GetComponent<ManageAttributes>().AttackedBy(other) <= 0) {
					ManageSound.Instance.PowerUpShieldDown();
					shieldActivate = false;
					shield.SetActive(false);
					shield.GetComponent<ManageAttributes>().RestartLife();
				}
			}
			else {
				// play sound
				ManageSound.Instance.ImpactShip();

				// show impact
				gameObject.GetComponent<Animator>().SetTrigger("start");

				// if i die
				if (gameObject.GetComponent<ManageAttributes>().AttackedBy(other) <= 0) {
					ManageGame.Instance.RemoveToLife(1);

					// last life, red flash
					int myLife = ManageGame.Instance.GetLife();
					if (myLife == 1) { gameObject.GetComponent<Animator>().SetTrigger("start"); }
					
					// if i have another life, i use it
					if (myLife > 0) { gameObject.GetComponent<ManageAttributes>().RestartLife(); }
					
					// else, game over
					else { ManageGame.Instance.StopGameLevel(); }
				}
			}
		}
		else if (other.tag == "powerUpLife") {
			ManageSound.Instance.PowerUpLife();
			ManageGame.Instance.AddToLife(other.GetComponent<ManageAttributes>().pointsToWin);
			Destroy(other);
		}
		else if (other.tag == "powerUpShield") {
			ManageSound.Instance.PowerUpShieldUp();
			shieldActivate = true;
			shield.SetActive(true);
			Destroy(other);
		}
	}

	private void OnCollisionExit2D(Collision2D collision) {
		if (ManageGame.Instance.GetLife() != 1) {
			gameObject.GetComponent<Animator>().SetTrigger("stop");
		}
	}
}
