﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Lasers/Move Laser 1")]
public class MoveLaser1 : MonoBehaviour {

	public float speed;
	private ViewBorders viewBorders;
		

	private void Start() {
		viewBorders = ViewBorders.Instance;
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
	}

	private void Update() {
		if (viewBorders.isOut(gameObject.transform.GetChild(0).gameObject)) {
			Destroy(gameObject);
		}
	}

	private void OnCollisionEnter2D(Collision2D collision) {
		ManageSound.Instance.ImpactMeteor();

		GameObject other = collision.gameObject;

		// show impact
		gameObject.transform.GetChild(0).gameObject.SetActive(false);
		gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
		gameObject.transform.GetChild(1).gameObject.SetActive(true);
		gameObject.transform.position = collision.GetContact(0).point;
		gameObject.AddComponent<DestructionEffectLaser>();

		if (other.tag != gameObject.tag) {
			// inflict damage
			int otherLife = other.GetComponent<ManageAttributes>().AttackedBy(gameObject);
			
			if (otherLife <= 0) {
				// collect points
				ManagerScore.Instance.AddToScore(other.GetComponent<ManageAttributes>().pointsToWin);

				// remove objects
				Destroy(other.GetComponent<PolygonCollider2D>());
				other.AddComponent<DestructionEffectEnemy>();
			}
		}

	}
}
