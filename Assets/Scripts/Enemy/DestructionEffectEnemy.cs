﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Enemy/Effect Destruction")]
public class DestructionEffectEnemy : MonoBehaviour {

	private Color color;


	private void Start(){
		color = new Color(1, 0, 0, 1);
		gameObject.GetComponent<SpriteRenderer>().color = color;
	}

	private void Update() {
		if (color.r < 1) { color.g += 0.03f; }
		if (color.g < 1) { color.g += 0.03f; }
		if (color.b < 1) { color.b += 0.03f; }
		if (color.a > 0) { color.a -= 0.03f; }
		gameObject.GetComponent<SpriteRenderer>().color = color;
		if (color.a <= 0) { Destroy(gameObject); }
	}
}
