﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Enemy/Manager Enemy")]
public class ManageEnemy : MonoBehaviour {

	[Header("Section Level")]
	public int sectionId;
	public float startTime;
	public float endTime;

	[Header("Generator")]
	public int minNumberOfEnemy;
	public int maxNumberOfEnemy;
	public GameObject prefabEnnemy;
	private System.Random generator;
	private ViewBorders viewBorders;


	private void Start() {
		generator = new System.Random();
		viewBorders = ViewBorders.Instance;
	}


	private void Update() {
		GameObject[] respawns = GameObject.FindGameObjectsWithTag(prefabEnnemy.tag);
		
		if (respawns.Length < maxNumberOfEnemy) {
			if (respawns.Length < minNumberOfEnemy) { BuildSomething(); }
			else if (generator.Next(0, 10) == 0) { BuildSomething(); }
		}
	}


	private void BuildSomething() {
		Vector3 size = prefabEnnemy.GetComponent<SpriteRenderer>().bounds.size;
		Instantiate(prefabEnnemy, RandomPosition(size.normalized), Quaternion.identity, gameObject.transform);
	}


	private Vector3 RandomPosition(Vector3 size) {
		Vector3 position = gameObject.transform.position;
		position.x = Random.Range(viewBorders.borderLeft() + size.x/2, viewBorders.borderRight() - size.x/2);
		position.y = viewBorders.borderTop() + size.y;
		return position;
	}
}
