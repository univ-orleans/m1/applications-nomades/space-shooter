﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Scripts/Enemy/Move Meteor")]
public class MoveMeteor : MonoBehaviour {

	public float speedMaxY;
	public bool enemy;
	private ViewBorders viewBorders;
	private Vector3 size;


	private void Start() {
		// initialise border view
		viewBorders = ViewBorders.Instance;

		size = gameObject.GetComponent<SpriteRenderer>().bounds.size;

		// initialize velocity
		Vector3 velocity = new Vector3(0, 0, 0);
		velocity.y = -Random.Range(0.5f, 1f) * speedMaxY;
		float distanceToLeft = viewBorders.borderRight() + gameObject.transform.position.x;
		float distanceToRight = viewBorders.borderRight() * 2 - distanceToLeft;
		float distanceToBottom = viewBorders.borderTop() * 2 + gameObject.transform.position.y;
		velocity.x = velocity.y * Random.Range(-distanceToLeft, distanceToRight) / -(viewBorders.borderTop() * 2);
		gameObject.GetComponent<Rigidbody2D>().velocity = velocity;
	}


	private void Update() {
		Vector3 position = gameObject.transform.position;
		if (position.y > viewBorders.borderTop() + size.y*2 || viewBorders.isOutFromBottom(gameObject) || viewBorders.isOutFromLeft(gameObject) ||viewBorders.isOutFromRight(gameObject)) {
			ManagerScore.Instance.RemoveToScore(gameObject.GetComponent<ManageAttributes>().pointsToWin);
			Destroy(gameObject);
		}
	}
}
